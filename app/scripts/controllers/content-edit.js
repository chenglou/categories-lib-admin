'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:ContentEditCtrl
 * @description
 * # ContentEditCtrl
 * Controller of the delegateApp
 */
var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('ContentEditCtrl', function($scope, $rootScope, $window, $routeParams) {

    $scope.pageClass = 'page-content-edit';

  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

  $scope.list = [];

     $scope.contentEditFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,

      confirmDelete: false,
      fieldsValid: false,

      popupConfirm: false,

      inputName: '',
      inputCategory: '',
      inputYear: '',
      inputFileName: '',
      inputFileURL: '',
      inputOtherURL: '',
      inputOrder: '',
      inputLive: '',
      inputDeleted: '',

      fileObject:''
    };

    $scope.brochureData = $rootScope.assetData.structure.objects[1];
    $scope.categoryData = $rootScope.assetData.structure.objects[0];
    $scope.categoryList = [];

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();
        $scope.getCategories();
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.contentEditFields.loading = false;
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list);
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();
      }
      $rootScope.resize();
    };



    $scope.getCategories = function() {
      var callback = function(response) {
        $scope.categoryList = [];
        if( response ) {
            var newObjs = [];
            if( response.objects.all) {
              for( var i = 0; i < response.objects.all.length; i++ ) {
                var item = response.objects.all[i];
                if( item.metafield.deleted && $rootScope.toBoolean(item.metafield.deleted.value) === false  ) {
                  // if( item.metafield.live && $rootScope.toBoolean(item.metafield.live.value) === true  ) {
                    newObjs.push(item);
                  // }
                }
              }
              newObjs.sort(function (a, b) {
      if (a.metadata.name < b.metadata.name) return -1;
      else if (a.metadata.name > b.metadata.name) return 1;
      return 0;
    });
              $scope.categoryList = newObjs;
              $scope.keywords = $scope.categoryList;
              console.log($scope.keywords);

            }
          }
        $rootScope.updateView();
      };
      $rootScope.cosmic.getObjects($scope.categoryData.type_slug,callback);
    };


    $scope.showPopup = function() {

    };


    // $scope.previewPDF = function(item) {
    //   console.log(item);
    //     // item.
    //   if( item ) {
    //     if( $rootScope.interact.config.user ) {
    //     $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "previewPDF-" + item);
    //     }
    //     window.location = item;
    //   }

    // };



    $scope.updateEdit = function(item) {
      // $scope.contentEditFields.loading = true;
      //  $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null || item === undefined ) {
        $scope.contentEditFields.inputName = '';
        $scope.contentEditFields.inputCategory = '';
        $scope.contentEditFields.inputYear = '';
        $scope.contentEditFields.inputFileName = '';
        $scope.contentEditFields.inputFileURL = '';
        $scope.contentEditFields.inputOtherURL = '';
        $scope.contentEditFields.inputOrder = '';
        $scope.contentEditFields.inputLive = true;
        $scope.contentEditFields.inputDeleted = false;
      }
      else {
        $scope.contentEditFields.inputId = item.slug;
        $scope.contentEditFields.inputName =      ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.contentEditFields.inputCategory =  ( item.metafield.category  ? item.metafield.category.value : '');
        $scope.contentEditFields.inputYear =      ( item.metafield.year      ? item.metafield.year.value : '');
        $scope.contentEditFields.inputFileName =  ( item.metafield.fileName  ? item.metafield.fileName.value : '');
        $scope.contentEditFields.inputFileURL =   ( item.metafield.fileURL   ? item.metafield.fileURL.value : '');
        $scope.contentEditFields.inputOtherURL =  ( item.metafield.otherURL  ? item.metafield.otherURL.value : '');
        $scope.contentEditFields.inputOrder =     ( item.metafield.order     ? item.metafield.order.value : '');
        $scope.contentEditFields.inputLive =      ( item.metafield.live      ? item.metafield.live.value : true );
        $scope.contentEditFields.inputDeleted =   ( item.metafield.deleted   ? item.metafield.deleted.value : false );
        console.log($scope.contentEditFields.inputCategory);
        console.log($scope.keywords);
        for (var i=0;i<$scope.keywords.length;i++){
          if ($scope.contentEditFields.inputCategory.indexOf($scope.keywords[i].slug) !== -1){
            $scope.keywords[i].checked = true;
          }
        }
      }
      $scope.contentEditFields.loading = false;
      $rootScope.rootFields.updating = false;
      $scope.validateFields();
      $rootScope.updateView();
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);

    };
    $scope.alertInvalidFields = function() {
      var memo = false;
          for (var i=0;i<$scope.keywords.length;i++){
          if ($scope.keywords[i].checked == true){
            var memo = true;
          }
        }

      if( $scope.contentEditFields.inputName === '' ) {
        window.alert('Please fill in Name');
      }
      // else if( $scope.contentEditFields.inputCategory === '' || $scope.contentEditFields.inputYear === null ) {
      //   window.alert('Please fill in Category');
      // }
      else if (!memo){
            window.alert('Please select a Category');
          }
      else if( $scope.contentEditFields.inputYear === '' || $scope.contentEditFields.inputYear === null ) {
        window.alert('Please fill in Year');
      }
      else if( $scope.contentEditFields.inputFileName === '' && $scope.contentEditFields.inputOtherURL === '' ) {
        window.alert('Please upload a PDF or a Video URL');
      }

      // else if( $scope.contentEditFields.inputOtherURL !== '' && $scope.contentEditFields.inputOtherURL.indexOf('http') === -1) {
      //   window.alert('Please fill in URL with http ');
      // }
    };

    $scope.validateFields = function() {
      var valid = true;

      console.log($scope.keywords);
      if ($scope.keywords != undefined){
        var valid = false;
      for (var i=0;i<$scope.keywords.length;i++){
      if ($scope.keywords[i].checked == true){
        valid = true;
      }
    }
  }

      if( $scope.contentEditFields.inputName === '' ) {
        valid = false;
      }
      console.log($scope.contentEditFields.inputYear);
      if( $scope.contentEditFields.inputYear === '' || $scope.contentEditFields.inputYear === null ) {
        valid = false;
      }
      // if( $scope.contentEditFields.inputOrder <= 0 ) {
      //   valid = false;
      // }
      if( $scope.contentEditFields.inputFileName === '' && $scope.contentEditFields.inputOtherURL === '' ) {
        valid = false;
      }

      // else if( $scope.contentEditFields.inputOtherURL !== '' && $scope.contentEditFields.inputOtherURL.indexOf('http') === -1) {
      //   valid = false;
      // }

      // if( $scope.contentEditFields.inputURL === '' || $scope.contentEditFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.contentEditFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      if( $scope.validateFields() ) {
        if( notify === undefined ) {
          notify = false;
        }
        $scope.contentEditFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.contentEditFields.inputId ) {
            $scope.contentEditFields.inputId = $scope.list ? $scope.list.length : 0;
            $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
        };
        var callback = function() {
          if( $scope.contentEditFields.fileObject ) {
            $scope.addMedia($scope.contentEditFields.fileObject, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.contentEditFields.filePDF ) {
          $scope.addMedia($scope.contentEditFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }
      }
      else {
        $scope.alertInvalidFields();
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.contentEditFields.filePDF = file;
        $scope.contentEditFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.contentEditFields.fileObject = file;
         $scope.contentEditFields.inputFileName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.contentEditFields.filePDF);
      // $window.console.log($scope.contentEditFields.fileObject);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.contentEditFields.inputFileURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.contentEditFields.inputFileURL = response.data.media.url;

        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };


  // $scope.keywords = [{name: "kw1"}, {name: "kw2"}]

    $scope.addObject = function(notify) {
      var checkedItems = [];
      for (var i=0;i<$scope.keywords.length;i++){
      if ($scope.keywords[i].checked == true){
        checkedItems.push($scope.keywords[i].slug);
      }
    }
    $scope.contentEditFields.inputCategory = checkedItems;
      var callback = function() {
        $scope.getObjects($scope.brochureData.type_slug);
        $window.location.href=$rootScope.loadPage('#/content');

        // $rootScope.loadPage('itinerary');
      };
      var data = {
        slug: $scope.brochureData.type_slug+'-'+$scope.contentEditFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields:[
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.contentEditFields.inputName },
            {
            'title': $scope.brochureData.object.category.title,
            'key': $scope.brochureData.object.category.key,
            'type': $scope.brochureData.object.category.type,
            'value': $scope.contentEditFields.inputCategory
            },
            { 'title': $scope.brochureData.object.year.title,       'key': $scope.brochureData.object.year.key,      'type': $scope.brochureData.object.year.type,      'value': $scope.contentEditFields.inputYear },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.contentEditFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.contentEditFields.inputFileURL },
            { 'title': $scope.brochureData.object.otherURL.title,    'key': $scope.brochureData.object.otherURL.key,   'type': $scope.brochureData.object.otherURL.type,   'value': $scope.contentEditFields.inputOtherURL },
            { 'title': $scope.brochureData.object.order.title,      'key': $scope.brochureData.object.order.key,     'type': $scope.brochureData.object.order.type,     'value': $scope.contentEditFields.inputOrder },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.contentEditFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.contentEditFields.inputDeleted },
        ]

        // $scope.contentEditFields.inputId = item.slug;
        // $scope.contentEditFields.inputName =      ( item.metafield.name      ? item.metafield.name.value : '');
        // $scope.contentEditFields.inputCategory =  ( item.metafield.category  ? item.metafield.category.value : '');
        // $scope.contentEditFields.inputYear =      ( item.metafield.year      ? item.metafield.year.value : '');
        // $scope.contentEditFields.inputFileName =  ( item.metafield.fileName  ? item.metafield.name.value : '');
        // $scope.contentEditFields.inputFileURL =   ( item.metafield.fileURL   ? item.metafield.fileURL.value : '');
        // $scope.contentEditFields.inputOtherURL =  ( item.metafield.otherURL  ? item.metafield.otherURL.value : '');
        // $scope.contentEditFields.inputOrder =     ( item.metafield.order     ? item.metafield.order.value : '');
        // $scope.contentEditFields.inputLive =      ( item.metafield.live      ? item.metafield.live.value : true );
        // $scope.contentEditFields.inputDeleted =   ( item.metafield.deleted   ? item.metafield.deleted.value : false );
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var checkedItems = [];
      for (var i=0;i<$scope.keywords.length;i++){
      if ($scope.keywords[i].checked == true){
        checkedItems.push($scope.keywords[i].slug);
      }
    }
    $scope.contentEditFields.inputCategory = checkedItems;
      var callback = function() {
        $scope.getObjects($scope.brochureData.type_slug);
        $window.location.href=$rootScope.loadPage('#/content');
      };
      var data = {
        slug: $scope.contentEditFields.inputId,
        metafields:[
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.contentEditFields.inputName },
            { 'title': $scope.brochureData.object.category.title,       'key': $scope.brochureData.object.category.key,      'type': $scope.brochureData.object.category.type,      'value': $scope.contentEditFields.inputCategory },
            { 'title': $scope.brochureData.object.year.title,       'key': $scope.brochureData.object.year.key,      'type': $scope.brochureData.object.year.type,      'value': $scope.contentEditFields.inputYear },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.contentEditFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.contentEditFields.inputFileURL },
            { 'title': $scope.brochureData.object.otherURL.title,    'key': $scope.brochureData.object.otherURL.key,   'type': $scope.brochureData.object.otherURL.type,   'value': $scope.contentEditFields.inputOtherURL },
            { 'title': $scope.brochureData.object.order.title,      'key': $scope.brochureData.object.order.key,     'type': $scope.brochureData.object.order.type,     'value': $scope.contentEditFields.inputOrder },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.contentEditFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.contentEditFields.inputDeleted },
        ]
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };

    $scope.toggleDelete = function() {
      $scope.contentEditFields.confirmDelete = !$scope.contentEditFields.confirmDelete;
    };

    $scope.deleteObject = function() {
       $window.console.log('itinerary.js, deleteObject();');
       $window.console.log($scope.contentEditFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.contentEditFields.inputId !== undefined && $scope.contentEditFields.inputId !== null && $scope.contentEditFields.inputId !== '' ){
          $scope.contentEditFields.inputDeleted = true;
          $scope.contentEditFields.inputLive = false;
          $scope.saveFields(false);
       }
    };




    $scope.getObjects = function() {
      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.contentEditFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.contentEditFields.disableButtons = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.contentEditFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));
          }
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        // $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

      $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});

var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('contentEditFilter', function() {
  return function(items, brochureData, rootFields) {

    // console.log('itineraryFilter');
    var debugFilter = false;
    var filtered = [];
    if( debugFilter ) {
      console.log(brochureData);
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;
        // console.log(item);
        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;
        }
        else{
          // console.log( item);
          if( debugFilter ) {

              console.log( 'item: is missing metafield.deleted, adding now');
          }

          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( rootFields.searchValue !== null ) {

            var text = rootFields.searchValue.toLowerCase();
          console.log('itineraryFilter: ' + rootFields.searchValue + ' ' + item.metafield.email.value + ' ' + text);

            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');
              result = true;
            }
            if (item.metafield.email.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');
              result = true;
            }
            else {
              result = false;
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }
    else {
      if( debugFilter ) {
        console.log('itineraryFilter: items not valid ' + JSON.stringify(items));
      }
    }
    return filtered;
  };
});
