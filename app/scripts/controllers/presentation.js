'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:PresentationCtrl
 * @description
 * # PresentationCtrl
 * Controller of the delegateApp
 */
var delegateApp;

angular.module('delegateApp')
  .controller('PresentationCtrl', function($scope, $rootScope, $window, $routeParams) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

    $scope.presentationFields = {
        editVisible: false,
        loading: true,
        disableButtons: false,
        sortName: true,
        sortStatus: null,
        confirmDelete: false,
        fieldsValid: false,

        inputId: '',
        inputName: '',
        inputMeeting: '',
        inputAuthor: '',
        inputDate: '',
        inputFileName: '',
        inputFileURL: '',
        inputImageName: '',
        inputImageURL: '',
        inputOrder: '',
        inputLive: '',
        inputDeleted: '',


        filePDF: '',
        fileImage:''
    };
   

    $scope.brochureData = {
        type:'presentations',
        title: "Presentation Library",
        logo: "brochure-presentations.png",
        type_slug: "presentation-objects",
        object: {
            name:       { "title":"Name",       "key":"name",       "type":"text", "value":"" },
            meeting:    { "title":"Meeting",    "key":"meeting",    "type":"text", "value":"" },
            author:     { "title":"Author",     "key":"author",     "type":"text", "value":"" },
            date:       { "title":"Date",       "key":"date",       "type":"text", "value":"" },
            fileName:   { "title":"File Name",  "key":"fileName",   "type":"text", "value":"" },
            fileURL:    { "title":"File URL",   "key":"fileURL",    "type":"text", "value":"" },
            imageName:  { "title":"Image Name", "key":"imageName",  "type":"text", "value":"" },
            imageURL:   { "title":"Image URL",  "key":"imageURL",   "type":"text", "value":"" },
            order:      { "title":"List order", "key":"order",      "type":"text", "value":"" },
            live:       { "title":"Status",     "key":"live",       "type":"text", "value":"" },
            deleted:    { "title":"Deleted",    "key":"deleted",    "type":"text", "value":"" }
        }
    };  

   $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();       
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.presentationFields.loading = false; 
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list); 
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();          
      }
      $rootScope.resize();
    };


  
    $scope.sortName = function() {
      // $scope.presentationFields.sortName = null;
      $scope.presentationFields.sortStatus = null;
      var sorted = [];
      $scope.presentationFields.sortName = !$scope.presentationFields.sortName;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.name.value.toLowerCase();
        var objB = b.metafield.name.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.presentationFields.sortName ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.presentationFields.sortName ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };
    $scope.sortStatus = function() {
      $scope.presentationFields.sortName = null;
      // $scope.presentationFields.sortStatus = null;
      var sorted = [];
      $scope.presentationFields.sortStatus = !$scope.presentationFields.sortStatus;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.presentationFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.presentationFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };


    $scope.updateEdit = function(item) {
       $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null ) {
        // $scope.presentationFields.inputId = $scope.list.length;
        $scope.presentationFields.inputName = '';
        $scope.presentationFields.inputFileName = '';
        $scope.presentationFields.inputFileURL = '';
        $scope.presentationFields.inputImageName = '';
        $scope.presentationFields.inputImageURL = '';
        $scope.presentationFields.inputOrder = '';
        $scope.presentationFields.inputLive = true;
        $scope.presentationFields.inputDeleted = false;
      }
      else {
        $scope.presentationFields.inputId = item.slug;
        $scope.presentationFields.inputName =        ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.presentationFields.inputMeeting =     ( item.metafield.meeting   ? item.metafield.meeting.value : ''); //item.metafield.job.value;
        $scope.presentationFields.inputAuthor =      ( item.metafield.author    ? item.metafield.author.value : ''); //item.metafield.department.value;
        $scope.presentationFields.inputDate =        ( item.metafield.date      ? item.metafield.date.value : ''); //item.metafield.mobile.value;
        $scope.presentationFields.inputFileName =    ( item.metafield.fileName  ? item.metafield.fileName.value : ''); //item.metafield.email.value;
        $scope.presentationFields.inputFileURL =     ( item.metafield.fileURL   ? item.metafield.fileURL.value : ''); //item.metafield.photoURL.value;
        $scope.presentationFields.inputImageName =   ( item.metafield.imageName ? item.metafield.imageName.value : ''); //item.metafield.photoFileName.value;
        $scope.presentationFields.inputImageURL =    ( item.metafield.imageURL  ? item.metafield.imageURL.value : ''); //item.metafield.address.value;
        $scope.presentationFields.inputOrder =       ( item.metafield.order     ? item.metafield.order.value : ''); //item.metafield.address.value;
        $scope.presentationFields.inputLive =        ( item.metafield.live      ? item.metafield.live.value : true ); //item.metafield.live.value;
        $scope.presentationFields.inputDeleted =     ( item.metafield.deleted   ? item.metafield.deleted.value : false ); //item.metafield.live.value;
      }
      $scope.presentationFields.loading = false;  
      $rootScope.rootFields.updating = false;  
      $scope.validateFields();   
      $rootScope.updateView();        
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);
        
    };


    $scope.validateFields = function() {
      var valid = true;
      if( $scope.presentationFields.inputName === '' ) {
        // TODO update with space removing string check
        // $window.alert('Name cannot be empty');
        valid = false;
      } 
      // if( $scope.presentationFields.inputURL === '' || $scope.presentationFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.presentationFields.fieldsValid = valid;
      return valid;
    };


    $scope.saveFields = function(notify) {
    if( $scope.validateFields() ) {
      if( notify === undefined ) {
        notify = false;
      }
    
        $scope.presentationFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.presentationFields.inputId ) {
          $scope.presentationFields.inputId = $scope.list ? $scope.list.length : 0;
          $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
          $rootScope.loadPage('presentations');
        };
        var callback = function() {
          if( $scope.presentationFields.fileImage ) {
            $scope.addMedia($scope.presentationFields.fileImage, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.presentationFields.filePDF ) {
           $scope.addMedia($scope.presentationFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }        
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.presentationFields.filePDF = file;
        $scope.presentationFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.presentationFields.fileImage = file;
         $scope.presentationFields.inputImageName = file.name;
                
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
       $rootScope.updateView();
      // $window.console.log($scope.presentationFields.filePDF);
      // $window.console.log($scope.presentationFields.fileImage);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.presentationFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.presentationFields.inputFileURL = response.data.media.url;
          
        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects('presentations');
         $rootScope.loadPage('presentations');        
      };
      var data = {
        slug: 'presentation-'+$scope.presentationFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.presentationFields.inputName },
            { 'title': $scope.brochureData.object.meeting.title,    'key': $scope.brochureData.object.meeting.key,   'type': $scope.brochureData.object.meeting.type,   'value': $scope.presentationFields.inputMeeting },
            { 'title': $scope.brochureData.object.author.title,     'key': $scope.brochureData.object.author.key,    'type': $scope.brochureData.object.author.type,    'value': $scope.presentationFields.inputAuthor },
            { 'title': $scope.brochureData.object.date.title,       'key': $scope.brochureData.object.date.key,      'type': $scope.brochureData.object.date.type,      'value': $scope.presentationFields.inputDate },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.presentationFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.presentationFields.inputFileURL },
            { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': $scope.presentationFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': $scope.presentationFields.inputImageURL },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.presentationFields.inputOrder },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.presentationFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.presentationFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('presentations');
         $rootScope.loadPage('presentations');   
      };
      var data = {
        slug: $scope.presentationFields.inputId,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.presentationFields.inputName },
            { 'title': $scope.brochureData.object.meeting.title,    'key': $scope.brochureData.object.meeting.key,   'type': $scope.brochureData.object.meeting.type,   'value': $scope.presentationFields.inputMeeting },
            { 'title': $scope.brochureData.object.author.title,     'key': $scope.brochureData.object.author.key,    'type': $scope.brochureData.object.author.type,    'value': $scope.presentationFields.inputAuthor },
            { 'title': $scope.brochureData.object.date.title,       'key': $scope.brochureData.object.date.key,      'type': $scope.brochureData.object.date.type,      'value': $scope.presentationFields.inputDate },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.presentationFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.presentationFields.inputFileURL },
            { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': $scope.presentationFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': $scope.presentationFields.inputImageURL },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.presentationFields.inputOrder },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.presentationFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.presentationFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.editObject(data, callback,notify);
    };
    
  $scope.toggleDelete = function() {
      $scope.presentationFields.confirmDelete = !$scope.presentationFields.confirmDelete;
      
    };

    $scope.deleteObject = function() {
       $window.console.log('tool.js, deleteObject();');
       $window.console.log($scope.presentationFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.presentationFields.inputId !== undefined && $scope.presentationFields.inputId !== null && $scope.presentationFields.inputId !== '' ){
          $scope.presentationFields.inputDeleted = true;
          $scope.presentationFields.inputLive = false;
          $scope.saveFields(false);
       }
    };
   $scope.getObjects = function() {
      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.presentationFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.presentationFields.disableButtons = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.presentationFields.loading = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));     
          }     
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

      $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});
  
var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('presentationFilter', function() {
  return function(items, brochureData, rootFields) {

    // console.log('presentationFilter');
    var debugFilter = false;
    var filtered = [];  
    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;
        // console.log(item);
        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;            
        } 
        else{
          // console.log( item);
          if( debugFilter ) {
           
              console.log( 'item: is missing metafield.deleted, adding now');             
          }
         
          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( rootFields.searchValue !== null ) {  
            
            var text = rootFields.searchValue.toLowerCase();
          console.log('presentationFilter: ' + rootFields.searchValue + ' ' + item.metafield.name.value + ' ' + text);
            
            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            else {
              result = false;
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('presentationFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});