'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:ContentCtrl
 * @description
 * # ContentCtrl
 * Controller of the delegateApp
 */
var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('ContentCtrl', function($scope, $rootScope, $window, $routeParams, $timeout) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

$scope.contentList = [];

    $window.console.log($rootScope.location.lastLocation);
    $window.console.log($rootScope.location.newLocation);

    if( $rootScope.location.lastLocation.indexOf('content') !== -1 || $rootScope.location.newLocation.indexOf('?search=') !== -1) {
    }

    if( $rootScope.location.lastLocation.indexOf('?search=') !== -1 || $rootScope.location.newLocation.indexOf('?search=') !== -1) {
      console.log('Remove page-content');
      $scope.pageClass = 'page-hide';
    }
    else {
      console.log('Add page-content');
      $scope.pageClass = 'page-content';
    }

    // $scope.pageClass = 'page-content';
    // $scope.$on('$locationChangeSuccess', function(event, newURL, oldURL){
    //   $window.console.log('Content Route Change');
    //   $window.console.log(newURL);
    //   $window.console.log(oldURL);
    //   if( oldURL.indexOf('?search=') !== -1 || newURL.indexOf('?search=') !== -1 ) {
    //     $window.console.log('REMOVE CLASS');
    //     $scope.pageClass = '';
    //   }
    // });

    // $window.console.log('add pageclass');

    $scope.list = [];

     $scope.contentFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      sortName: true,
      sortEmail: null,
      sortStatus: null,
      confirmDelete: false,
      fieldsValid: false,

      inputName: '',
      inputCategory: '',
      inputYear: '',
      inputFileName: '',
      inputFileURL: '',
      inputOtherURL: '',
      inputOrder: '',
      inputLive: '',
      inputDeleted: '',

      filePDF: '',
      fileImage:''
    };
    // console.log($rootScope.assetData.structure.objects[0]);
     $scope.brochureData = $rootScope.assetData.structure.objects[1];
    //  {
    //   type:'itineraries',
    //   title: "All Staff Itineraries",
    //   logo: "brochure-roster.png",
    //   type_slug: "apac-itineraries",
    //   object: {
    //         name:       { "title":"Name",       "key":"name",       "type":"text", "value":"" },
    //         email:      { "title":"Email",      "key":"email",      "type":"text", "value":"" },
    //         fileName:   { "title":"File Name",  "key":"fileName",   "type":"text", "value":"" },
    //         fileURL:    { "title":"File URL",   "key":"fileURL",    "type":"text", "value":"" },
    //         live:       { "title":"Live",       "key":"live",       "type":"text", "value":"" },
    //         deleted:    { "title":"Deleted",    "key":"deleted",    "type":"text", "value":"" }
    //     }
    // };

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.contentFields.loading = false;
        $timeout(function() {
          $rootScope.rootFields.updating = false;
        }, 500);
        // $window.console.log($scope.list);
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();
      }
      // $rootScope.resize();
    };

    $scope.sortName = function() {
      if($rootScope.data.content) {
      // $scope.contentFields.sortName = null;
      $scope.contentFields.sortEmail = null;
      $scope.contentFields.sortStatus = null;

      var sorted = [];
      $scope.contentFields.sortName = !$scope.contentFields.sortName;
      sorted = $rootScope.data.content.sort(function(a,b) {
        var objA = a.metafield.name.value.toLowerCase();
        var objB = b.metafield.name.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.contentFields.sortName ) {
            return -1;
          }
          else {
             return 1;
          }
        }
        else if( objA > objB) {
          if( $scope.contentFields.sortName ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      // $scope.list = sorted;
    }

    };
    $scope.sortEmail = function() {
      if( $scope.list ) {
      $scope.contentFields.sortName = null;
      // $scope.contentFields.sortEmail = null;
      $scope.contentFields.sortStatus = null;

      var sorted = [];
      $scope.contentFields.sortEmail = !$scope.contentFields.sortEmail;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.email.value.toLowerCase();
        var objB = b.metafield.email.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.contentFields.sortEmail ) {
            return -1;
          }
          else {
             return 1;
          }

        }
        else if( objA > objB) {
          if( $scope.contentFields.sortEmail ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.sortStatus = function() {
      if( $rootScope.data.content ) {
      $scope.contentFields.sortName = null;
      $scope.contentFields.sortEmail = null;
      // $scope.contentFields.sortStatus = null;
      var sorted = [];
      $scope.contentFields.sortStatus = !$scope.contentFields.sortStatus;
      sorted = $rootScope.data.content.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.contentFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }

        }
        else if( objA > objB) {
          if( $scope.contentFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.previewPDF = function(item) {
      // console.log(item);
        // item.
      if( item ) {
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "previewPDF-" + item);
        }
        window.location = item;
      }

    };



    $scope.updateEdit = function(item) {
      // $scope.contentFields.loading = true;
      //  $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null || item === undefined ) {
        $scope.contentFields.inputName = '';
        $scope.contentFields.inputCategory = '';
        $scope.contentFields.inputYear = '';
        $scope.contentFields.inputFileName = '';
        $scope.contentFields.inputFileURL = '';
        $scope.contentFields.inputOtherURL = '';
        $scope.contentFields.inputOrder = '';
        $scope.contentFields.inputLive = true;
        $scope.contentFields.inputDeleted = false;
      }
      else {
        $scope.contentFields.inputId = item.slug;
        $scope.contentFields.inputName =      ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.contentFields.inputCategory =  ( item.metafield.category  ? item.metafield.category.value : '');
        $scope.contentFields.inputYear =      ( item.metafield.year      ? item.metafield.year.value : '');
        $scope.contentFields.inputFileName =  ( item.metafield.fileName  ? item.metafield.fileName.value : '');
        $scope.contentFields.inputFileURL =   ( item.metafield.fileURL   ? item.metafield.fileURL.value : '');
        $scope.contentFields.inputOtherURL =  ( item.metafield.otherURL  ? item.metafield.otherURL.value : '');
        $scope.contentFields.inputOrder =     ( item.metafield.order     ? item.metafield.order.value : '');
        $scope.contentFields.inputLive =      ( item.metafield.live      ? item.metafield.live.value : true );
        $scope.contentFields.inputDeleted =   ( item.metafield.deleted   ? item.metafield.deleted.value : false );
      }
      $scope.contentFields.loading = false;

      $timeout(function() {
        $rootScope.rootFields.updating = false;
      }, 500);

      $scope.validateFields();
      $rootScope.updateView();
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);

    };


    $scope.validateFields = function() {
      var valid = true;
      if( $scope.contentFields.inputName === '' ) {
        valid = false;
      }
      console.log($scope.contentFields.inputYear);
      if( $scope.contentFields.inputYear === '' || $scope.contentFields.inputYear === null ) {
        valid = false;
      }
      // if( $scope.contentFields.inputOrder <= 0 ) {
      //   valid = false;
      // }
      if( $scope.contentFields.inputFileName === '' && $scope.contentFields.inputOtherURL === '' ) {
        valid = false;
      }

      // if( $scope.contentFields.inputURL === '' || $scope.contentFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.contentFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      if( $scope.validateFields() ) {
        if( notify === undefined ) {
          notify = false;
        }
        $scope.contentFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.contentFields.inputId ) {
            $scope.contentFields.inputId = $scope.list ? $scope.list.length : 0;
            $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
        };
        var callback = function() {
          if( $scope.contentFields.fileImage ) {
            $scope.addMedia($scope.contentFields.fileImage, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.contentFields.filePDF ) {
          $scope.addMedia($scope.contentFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.contentFields.filePDF = file;
        $scope.contentFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.contentFields.fileImage = file;
         $scope.contentFields.inputImageName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.contentFields.filePDF);
      // $window.console.log($scope.contentFields.fileImage);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.contentFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.contentFields.inputFileURL = response.data.media.url;

        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects($scope.brochureData.type_slug);
        $window.location.href=$rootScope.loadPage('#/content');
      };
      var data = {
        slug: 'dmr-content-'+$scope.contentFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.contentFields.inputName },
            { 'title': $scope.brochureData.object.category.title,      'key': $scope.brochureData.object.category.key,     'type': $scope.brochureData.object.category.type,     'value': $scope.contentFields.inputCategory },
            { 'title': $scope.brochureData.object.year.title,      'key': $scope.brochureData.object.year.key,     'type': $scope.brochureData.object.year.type,     'value': $scope.contentFields.inputYear },
            { 'title': $scope.brochureData.object.order.title,      'key': $scope.brochureData.object.order.key,     'type': $scope.brochureData.object.order.type,     'value': $scope.contentFields.inputOrder },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.contentFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.contentFields.inputFileURL },
            { 'title': $scope.brochureData.object.otherURL.title,    'key': $scope.brochureData.object.otherURL.key,   'type': $scope.brochureData.object.otherURL.type,   'value': $scope.contentFields.inputOtherURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.contentFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.contentFields.inputDeleted },
        ]
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('itineraries');
        $window.location.href=$rootScope.loadPage('#/content');
      };
      var data = {
        slug: $scope.contentFields.inputId,
        metafields: [
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.contentFields.inputName },
            { 'title': $scope.brochureData.object.category.title,      'key': $scope.brochureData.object.category.key,     'type': $scope.brochureData.object.category.type,     'value': $scope.contentFields.inputCategory },
            { 'title': $scope.brochureData.object.year.title,      'key': $scope.brochureData.object.year.key,     'type': $scope.brochureData.object.year.type,     'value': $scope.contentFields.inputYear },
            { 'title': $scope.brochureData.object.order.title,      'key': $scope.brochureData.object.order.key,     'type': $scope.brochureData.object.order.type,     'value': $scope.contentFields.inputOrder },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.contentFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.contentFields.inputFileURL },
            { 'title': $scope.brochureData.object.otherURL.title,    'key': $scope.brochureData.object.otherURL.key,   'type': $scope.brochureData.object.otherURL.type,   'value': $scope.contentFields.inputOtherURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.contentFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.contentFields.inputDeleted },
        ]
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };

    $scope.toggleDelete = function() {
      $scope.contentFields.confirmDelete = !$scope.contentFields.confirmDelete;
    };

    $scope.deleteObject = function() {
       $window.console.log('itinerary.js, deleteObject();');
       $window.console.log($scope.contentFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.contentFields.inputId !== undefined && $scope.contentFields.inputId !== null && $scope.contentFields.inputId !== '' ){
          $scope.contentFields.inputDeleted = true;
          $scope.contentFields.inputLive = false;
          $scope.saveFields(false);
       }
    };


    $scope.getObjects = function() {
      // console.log('$scope.getObjects();');
      // console.log($scope.brochureData);

      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.contentFields.loading = false;
        $timeout(function() {
          $rootScope.rootFields.updating = false;
        }, 500);
        $scope.contentFields.disableButtons = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.contentFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));
          }
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        // $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

      $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});

var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('contentFilter', function() {
  return function(items, filters, fields) {
    console.log('CONTENT FILTER');
    console.log(items);
    console.log(filters);
    console.log(fields);
    // console.log('itineraryFilter');
    var debugFilter = false;
    var filtered = [];
    if( debugFilter ) {
      console.log(brochureData);
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;

        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;
        }
        else{
          for( var f = 0; f < filters.length; f++ ) {
            console.log('filter' + filters[f]);
            if( filters[f] === item.metafield.category.value ) {
              console.log('filter match');
              result = false;
            }
          }
          if( result ) {
            if( fields && fields.searchValue ) {
              var text = fields.searchValue.toLowerCase();
              console.log(text);
              if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
                console.log('test');
                result = true;
              }
              else {
                result = false;
              }
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }
    return filtered;
  };
});
