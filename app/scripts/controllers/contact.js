'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the delegateApp
 */

var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('ContactCtrl', function ($scope, $rootScope, $window, $routeParams) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

    $scope.list = [];

    $scope.contactFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      sortName: true,
      sortStatus: null,
      sortJob: null,
      confirmDelete: false,
      fieldsValid: false,

      inputId: '',
      inputName: '',
      inputJob: '',
      inputDepartment: '',
      inputMobile: '',
      inputEmail: '',
      inputPhotoURL: '',
      inputPhotoFileName: '',
      inputLive: '',
      inputPracticeName: '',
      inputAddress:'',
      inputSuburb: '',
      inputPostCode: '',
      inputState: '',
      inputPhone:'',
      inputFax:'',
      
      filePhoto: '',      
    };
   
    
    $scope.brochureData = {
      type:'contacts',
      title: "Staff Contact List",
      logo: "brochure-contacts.png",
      type_slug: "contact-objects",
      object: {
        userID: { 'title':'userID',     'key':'userID',     'type':'text',  'value':'' },
        name: { "title":"Name",           "key":"name",           "type":"text",  "value":"" },
        job:  { "title":"Job Title",      "key":"job",            "type":"text",  "value":"" },
        department:  { "title":"Department",     "key":"department",     "type":"text",  "value":"" },
        mobile:  { "title":"Mobile",         "key":"mobile",         "type":"text",  "value":"" },
        email:  { "title":"Email",          "key":"email",          "type":"text",  "value":"" },
        photoURL:  { "title":"Photo url",      "key":"photoURL",       "type":"text",  "value":"" },
        photoFileName:  { "title":"Photo filename", "key":"photoFileName",  "type":"text",  "value":"" },
        practiceName:  { "title":"Practice Name",        "key":"practiceName",        "type":"text",  "value":"" },
        address:  { "title":"Address",        "key":"address",        "type":"text",  "value":"" },
        suburb:  { "title":"Suburb",         "key":"suburb",         "type":"text",  "value":"" },
        postCode:  { "title":"Post Code",      "key":"postCode",       "type":"text",  "value":"" },
        state:  { "title":"State",          "key":"state",          "type":"text",  "value":"" },
        phone:  { "title":"Phone",          "key":"phone",          "type":"text",  "value":"" },
        fax:  { "title":"Fax",            "key":"fax",            "type":"text",  "value":"" },
        live:  { "title":"Status",         "key":"live",           "type":"text",  "value":"" },
        deleted:  { "title":"Deleted",        "key":"deleted",        "type":"text",  "value":"" },
        pending:  { "title":"Pending",        "key":"pending",        "type":"text",  "value":"" },
      },
      pending: [],
    };

     


   $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $window.console.log('getObjects(); edit');
        $scope.getObjects();       
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.contactFields.loading = false; 
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list); 
        $scope.list = $rootScope.lastList;
      }
      else {
        $window.console.log('getObjects(); else');
        
        $scope.getObjects();    
        $scope.validateObjects();      
      }
      $rootScope.resize();
    };

    $scope.validateObjects = function() {
      $window.console.log('validateObjects();');
      for( var i = 0; i < $scope.list.length; i++ ) {
        $window.console.log($scope.list[i]);
        // if( item.metafield.userID ) { 

        // }
        // else {
        //   $window.console.log(item.slug + ' add metafield.userID');
        //   // item.metafield.userID = $scope.brochureData.object.userID;

        // }
      }
    };

     $scope.sortName = function() {
      // $scope.contactFields.sortName = null;
      $scope.contactFields.sortJob = null;
      $scope.contactFields.sortStatus = null;

      var sorted = [];
      $scope.contactFields.sortName = !$scope.contactFields.sortName;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.name.value.toLowerCase();
        var objB = b.metafield.name.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.contactFields.sortName ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.contactFields.sortName ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };
     $scope.sortJob = function() {
      $scope.contactFields.sortName = null;
      // $scope.contactFields.sortJob = null;
      $scope.contactFields.sortStatus = null;
      var sorted = [];
      $scope.contactFields.sortJob = !$scope.contactFields.sortJob;
      sorted = $scope.list.sort(function(a,b) {
        var objA =  a.metafield.job.value !== null ? a.metafield.job.value.toLowerCase() : '';
        var objB = b.metafield.job.value !== null ? b.metafield.job.value.toLowerCase() : '';
        if( objA < objB ) {
          if( $scope.contactFields.sortJob ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.contactFields.sortJob ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };
    $scope.sortStatus = function() {
      $scope.contactFields.sortName = null;
      $scope.contactFields.sortJob = null;
      // $scope.contactFields.sortStatus = null;
      var sorted = [];
      $scope.contactFields.sortStatus = !$scope.contactFields.sortStatus;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.contactFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.contactFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };

    $scope.updateEdit = function(item) {
      //  $window.console.log('updateEdit()' + JSON.stringify(item)); 
      if( item === null ) {
        // $scope.contactFields.inputId = $scope.list.length;
        $scope.contactFields.inputName = '';
        $scope.contactFields.inputJob = '';
        $scope.contactFields.inputDepartment = '';
        $scope.contactFields.inputMobile = '';
        $scope.contactFields.inputEmail = '';
        $scope.contactFields.inputPhotoURL = '';
        $scope.contactFields.inputPhotoFileName = '';
        $scope.contactFields.inputPracticeName = '';
        $scope.contactFields.inputAddress = '';
        $scope.contactFields.inputSuburb = '';
        $scope.contactFields.inputPostCode = '';
        $scope.contactFields.inputState = '';
        $scope.contactFields.inputPhone = '';
        $scope.contactFields.inputFax = '';
        $scope.contactFields.inputLive = true;
        $scope.contactFields.inputDeleted = false;
        $scope.contactFields.inputPending = false;
      }
      else {
        $scope.contactFields.inputId = item.slug;
        $scope.contactFields.inputName =          ( item.metafield.name  ?          item.metafield.name.value : '');
        $scope.contactFields.inputJob =           ( item.metafield.job ?            item.metafield.job.value : ''); //item.metafield.job.value;
        $scope.contactFields.inputDepartment =    ( item.metafield.department ?     item.metafield.department.value : ''); //item.metafield.department.value;
        $scope.contactFields.inputMobile =        ( item.metafield.mobile ?         item.metafield.mobile.value : ''); //item.metafield.mobile.value;
        $scope.contactFields.inputEmail =         ( item.metafield.email ?          item.metafield.email.value : ''); //item.metafield.email.value;
        $scope.contactFields.inputPhotoURL =      ( item.metafield.photoURL ?       item.metafield.photoURL.value : ''); //item.metafield.photoURL.value;
        $scope.contactFields.inputPhotoFileName = ( item.metafield.photoFileName ?  item.metafield.photoFileName.value : ''); //item.metafield.photoFileName.value;
        $scope.contactFields.inputPracticeName =       ( item.metafield.practiceName ?        item.metafield.practiceName.value : ''); //item.metafield.address.value;
        $scope.contactFields.inputAddress =       ( item.metafield.address ?        item.metafield.address.value : ''); //item.metafield.address.value;
        $scope.contactFields.inputSuburb =        ( item.metafield.suburb ?         item.metafield.suburb.value : ''); //item.metafield.suburb.value;
        $scope.contactFields.inputPostCode =      ( item.metafield.postCode ?       item.metafield.postCode.value : '');//item.metafield.postCode.value;
        $scope.contactFields.inputState =         ( item.metafield.state ?          item.metafield.state.value : ''); //item.metafield.state.value;
        $scope.contactFields.inputPhone =         ( item.metafield.phone ?          item.metafield.phone.value : ''); //item.metafield.phone.value;
        $scope.contactFields.inputFax =           ( item.metafield.fax ?            item.metafield.fax.value : '');// item.metafield.fax.value;
        $scope.contactFields.inputLive =          ( item.metafield.live ?           item.metafield.live.value : true); //item.metafield.live.value;
        $scope.contactFields.inputDeleted =          ( item.metafield.deleted ?           item.metafield.deleted.value : false); //item.metafield.live.value;
        $scope.contactFields.inputPending  =          ( item.metafield.pending  ?           item.metafield.pending.value : false); //item.metafield.live.value;
      }
      $scope.contactFields.loading = false;    
      $rootScope.rootFields.updating = false;   
       $scope.validateFields();   
      $rootScope.updateView();        
    };



    $scope.switchSave = function(item) {
        item.metafield.pending = false;
        $scope.updateEdit(item);
        $scope.saveFields(false);
        
    };
     $scope.validateFields = function() {
       $window.console.log('validateFields();');
      var valid = true;
      if( $scope.contactFields.inputName === '' ) {
        valid = false;
      } 
  

      $scope.contactFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      if( $scope.validateFields() ) {
        if( notify === undefined) {
          notify = false;
        }
        $scope.contactFields.disableButtons = true;
      
        if( $scope.contactFields.inputLive ) 
        {
          $scope.contactFields.inputPending = false;
        }

        $window.console.log('callback start()');
        var callbackAdd = function() {
          if( !$scope.contactFields.inputId ) {
          $scope.contactFields.inputId = $scope.list ? $scope.list.length : 0;
          $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
          $rootScope.loadPage('contacts');
        };
        var callback = function() {
          if( $scope.contactFields.filePhoto ) {
            $scope.addMedia($scope.contactFields.filePhoto, 'image', callbackAdd ) ;
          }
          else {
            callbackAdd();
          }
        };
        callback();
        // if( $scope.contactFields.filePDF ) {
        //    $scope.addMedia($scope.contactFields.filePDF, 'pdf', callback);
        // }
        // else {
        //   callback();
        // }        
      }
    };
  

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.contactFields.filePhoto = file;
         $scope.contactFields.inputPhotoFileName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
    };

     $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          var urlString = response.data.media.url.replace(/ /g, '%20');
          $scope.contactFields.inputPhotoURL = urlString;
        }
        // else if( field === 'pdf' ) {
        //   $scope.presentationFields.inputFile = response.data.media.url;
          
        // }
        else {
          $window.alert('invalid addMedia() field');
        }
        if( callback ) {
          _callback();          
        }
      };
      $rootScope.cosmic.addMedia(file,callback);
    };


    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects($scope.brochureData.type);
        $rootScope.loadPage($scope.brochureData.type);     
      };
      var data = {
        slug: 'contact-'+$scope.contactFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [
          { "title":$scope.brochureData.object.name.title,            "key":$scope.brochureData.object.name.key,            "type":$scope.brochureData.object.name.type,          "value": $scope.contactFields.inputName },
          { "title":$scope.brochureData.object.job.title,             "key":$scope.brochureData.object.job.key,             "type":$scope.brochureData.object.job.type,           "value": $scope.contactFields.inputJob },
          { "title":$scope.brochureData.object.department.title,      "key":$scope.brochureData.object.department.key,      "type":$scope.brochureData.object.department.type,    "value": $scope.contactFields.inputDepartment },
          { "title":$scope.brochureData.object.mobile.title,          "key":$scope.brochureData.object.mobile.key,          "type":$scope.brochureData.object.mobile.type,        "value": $scope.contactFields.inputMobile },
          { "title":$scope.brochureData.object.email.title,           "key":$scope.brochureData.object.email.key,           "type":$scope.brochureData.object.email.type,         "value": $scope.contactFields.inputEmail },
          { "title":$scope.brochureData.object.photoURL.title,        "key":$scope.brochureData.object.photoURL.key,        "type":$scope.brochureData.object.photoURL.type,      "value": $scope.contactFields.inputPhotoURL },
          { "title":$scope.brochureData.object.photoFileName.title,   "key":$scope.brochureData.object.photoFileName.key,   "type":$scope.brochureData.object.photoFileName.type, "value": $scope.contactFields.inputPhotoFileName},
          { "title":$scope.brochureData.object.practiceName.title,         "key":$scope.brochureData.object.practiceName.key,         "type":$scope.brochureData.object.practiceName.type,       "value": $scope.contactFields.inputPracticeName },
          { "title":$scope.brochureData.object.address.title,         "key":$scope.brochureData.object.address.key,         "type":$scope.brochureData.object.address.type,       "value": $scope.contactFields.inputAddress },
          { "title":$scope.brochureData.object.suburb.title,         "key":$scope.brochureData.object.suburb.key,         "type":$scope.brochureData.object.suburb.type,       "value": $scope.contactFields.inputSuburb },
          { "title":$scope.brochureData.object.postCode.title,        "key":$scope.brochureData.object.postCode.key,        "type":$scope.brochureData.object.postCode.type,      "value": $scope.contactFields.inputPostCode },
          { "title":$scope.brochureData.object.state.title,           "key":$scope.brochureData.object.state.key,           "type":$scope.brochureData.object.state.type,         "value": $scope.contactFields.inputState },
          { "title":$scope.brochureData.object.phone.title,           "key":$scope.brochureData.object.phone.key,           "type":$scope.brochureData.object.phone.type,         "value": $scope.contactFields.inputPhone },
          { "title":$scope.brochureData.object.fax.title,             "key":$scope.brochureData.object.fax.key,             "type":$scope.brochureData.object.fax.type,           "value": $scope.contactFields.inputFax },
          { "title":$scope.brochureData.object.live.title,            "key":$scope.brochureData.object.live.key,            "type":$scope.brochureData.object.live.type,          "value": $scope.contactFields.inputLive },
          { "title":$scope.brochureData.object.deleted.title,         "key":$scope.brochureData.object.deleted.key,         "type":$scope.brochureData.object.deleted.type,       "value": $scope.contactFields.inputDeleted },
          { "title":$scope.brochureData.object.pending.title,         "key":$scope.brochureData.object.pending.key,         "type":$scope.brochureData.object.email.type,         "value": $scope.contactFields.inputPending },
        ]
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects($scope.brochureData.type);  
        $rootScope.loadPage($scope.brochureData.type);     
      };
      var data = {
        slug: $scope.contactFields.inputId,
        metafields: [
          { "title":$scope.brochureData.object.name.title,            "key":$scope.brochureData.object.name.key,            "type":$scope.brochureData.object.name.type,          "value": $scope.contactFields.inputName },
          { "title":$scope.brochureData.object.job.title,             "key":$scope.brochureData.object.job.key,             "type":$scope.brochureData.object.job.type,           "value": $scope.contactFields.inputJob },
          { "title":$scope.brochureData.object.department.title,      "key":$scope.brochureData.object.department.key,      "type":$scope.brochureData.object.department.type,    "value": $scope.contactFields.inputDepartment },
          { "title":$scope.brochureData.object.mobile.title,          "key":$scope.brochureData.object.mobile.key,          "type":$scope.brochureData.object.mobile.type,        "value": $scope.contactFields.inputMobile },
          { "title":$scope.brochureData.object.email.title,           "key":$scope.brochureData.object.email.key,           "type":$scope.brochureData.object.email.type,         "value": $scope.contactFields.inputEmail },
          { "title":$scope.brochureData.object.photoURL.title,        "key":$scope.brochureData.object.photoURL.key,        "type":$scope.brochureData.object.photoURL.type,      "value": $scope.contactFields.inputPhotoURL },
          { "title":$scope.brochureData.object.photoFileName.title,   "key":$scope.brochureData.object.photoFileName.key,   "type":$scope.brochureData.object.photoFileName.type, "value": $scope.contactFields.inputPhotoFileName},
          { "title":$scope.brochureData.object.practiceName.title,    "key":$scope.brochureData.object.practiceName.key,         "type":$scope.brochureData.object.practiceName.type,       "value": $scope.contactFields.inputPracticeName },          
          { "title":$scope.brochureData.object.address.title,         "key":$scope.brochureData.object.address.key,         "type":$scope.brochureData.object.address.type,       "value": $scope.contactFields.inputAddress },
          { "title":$scope.brochureData.object.suburb.title,          "key":$scope.brochureData.object.suburb.key,         "type":$scope.brochureData.object.suburb.type,       "value": $scope.contactFields.inputSuburb },
          { "title":$scope.brochureData.object.postCode.title,        "key":$scope.brochureData.object.postCode.key,        "type":$scope.brochureData.object.postCode.type,      "value": $scope.contactFields.inputPostCode },
          { "title":$scope.brochureData.object.state.title,           "key":$scope.brochureData.object.state.key,           "type":$scope.brochureData.object.state.type,         "value": $scope.contactFields.inputState },
          { "title":$scope.brochureData.object.phone.title,           "key":$scope.brochureData.object.phone.key,           "type":$scope.brochureData.object.phone.type,         "value": $scope.contactFields.inputPhone },
          { "title":$scope.brochureData.object.fax.title,             "key":$scope.brochureData.object.fax.key,             "type":$scope.brochureData.object.fax.type,           "value": $scope.contactFields.inputFax },
          { "title":$scope.brochureData.object.live.title,            "key":$scope.brochureData.object.live.key,            "type":$scope.brochureData.object.live.type,          "value": $scope.contactFields.inputLive },
          { "title":$scope.brochureData.object.deleted.title,         "key":$scope.brochureData.object.deleted.key,         "type":$scope.brochureData.object.deleted.type,       "value": $scope.contactFields.inputDeleted },
          { "title":$scope.brochureData.object.pending.title,         "key":$scope.brochureData.object.pending.key,         "type":$scope.brochureData.object.email.type,         "value": $scope.contactFields.inputPending },
        ]
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };
    
    $scope.toggleDelete = function() {
      $scope.contactFields.confirmDelete = !$scope.contactFields.confirmDelete;
    };
    $scope.deleteObject = function() {
       $window.console.log('tool.js, deleteObject();');
       $window.console.log($scope.contactFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.contactFields.inputId !== undefined && $scope.contactFields.inputId !== null && $scope.contactFields.inputId !== '' ){
          $scope.contactFields.inputDeleted = true;
          $scope.contactFields.inputLive = false;
          $scope.saveFields(false);
       } 
    };

    $scope.getObjects = function() {
      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.contactFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.contactFields.disableButtons = false;

        // $scope.brochureData.pending = [];

        // for( var i = 0; i < list.length; i++ ) {
        //   if( list[i].metafield.pending && list[i].metafield.pending.value ) {
        //     $
        //   }
        // }

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.contactFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);            
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));     
          }     
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug, callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

    // $scope.getObjects();
    $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});
  
var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('contactFilter', function() {
  return function(items, brochureData, rootFields) {

    var debugFilter = false;
    var filtered = [];  
    var text = '';

    brochureData.pending = [];

    if( rootFields.searchValue ) {
      text = rootFields.searchValue.toString().toLowerCase();                
    }
    console.log(text);

    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = false;

        if( item.metafield.deleted && toBoolean( item.metafield.deleted.value )) {
          result = false;            
        } 
        else {
          if( debugFilter ) {
             console.log( item);
              console.log( 'item: is missing metafield.deleted, adding now');             
          }
         
          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( !item.metafield.practiceName ) {
            item.metafield.practiceName = {
            key: brochureData.object.practiceName.key,
            title: brochureData.object.practiceName.title,
            type: brochureData.object.practiceName.type,
            value: ''
          };
          }
        }
        
        if( text === '' ){
          result = true;
        }
        else {
          if( item.metafield.name && item.metafield.name.value ) {
              if( item.metafield.name.value.toString().toLowerCase().indexOf(text) > -1) { result = true;  }                            
          }
          if( item.metafield.job && item.metafield.job.value ) {
              if( item.metafield.job.value.toString().toLowerCase().indexOf(text) > -1) { result = true; }                      
          }
          if( item.metafield.department && item.metafield.department.value ) {
              if( item.metafield.department.value.toString().toLowerCase().indexOf(text) > -1) { result = true; }                            
          }
        }
        if( result && item.metafield.pending && toBoolean( item.metafield.pending.value )) {
            // console.log('Pending object added ');
            // console.log(item);
            result = false;     
            brochureData.pending.push(item);       
        }

        if( result === true ) {
          console.log('Added item ' + item.metafield.name.value + ' ' + text);
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('rosterFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});


delegateApp.filter('contactPending', function() {
  return function(items, brochureData) {

    var debugFilter = false;
    var filtered = [];  
    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = false;
        if( item.metafield.pending ) {
          if( toBoolean( item.metafield.pending.value ) ) {
            result = true;            
          }
        } 
        if( result ) {
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('rosterFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});

