'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:ToolCtrl
 * @description
 * # ToolCtrl
 * Controller of the delegateApp
 */
var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('ToolCtrl', function($scope, $rootScope, $window, $routeParams) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {
    $scope.list = [];

    $scope.toolFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      sortName: true,
      sortStatus: null,
      confirmDelete: false,
      fieldsValid: false,

      inputId: '',
      inputName: '',
      inputDate: '',
      inputDescription: '',
      inputURL: '',
      inputOrder: '',
      inputLive: '',
      inputDeleted: '',

    };

     $scope.brochureData = {
      type:'tools',
      title: "All Staff Tools",
      logo: "brochure-tools.png",
      type_slug: "tool-objects",
      object: {
            name:          { "title":"Name",         "key":"name",         "type":"text", "value":"" },
            date:          { "title":"Date",         "key":"date",         "type":"text", "value":"" },
            description:   { "title":"Description",  "key":"description",  "type":"text", "value":"" },
            url:           { "title":"URL",          "key":"url",          "type":"text", "value":"" },
            order:      { "title":"List order", "key":"order",      "type":"text", "value":"" },
            live:       { "title":"Status",     "key":"live",       "type":"text", "value":"" },
            deleted:    { "title":"Deleted",    "key":"deleted",       "type":"text", "value":"" }
        }
    };

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();       
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.toolFields.loading = false; 
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list); 
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();          
      }
      $rootScope.resize();
    };


    $scope.sortName = function() {
      // $scope.toolFields.sortName = null;
      $scope.toolFields.sortStatus = null;

      var sorted = [];
      $scope.toolFields.sortName = !$scope.toolFields.sortName;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.name.value.toLowerCase();
        var objB = b.metafield.name.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.toolFields.sortName ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.toolFields.sortName ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };
    $scope.sortURL = function() {
      var sorted = [];
      $scope.toolFields.sortURL = !$scope.toolFields.sortURL;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.url.value.toLowerCase();
        var objB = b.metafield.url.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.toolFields.sortURL ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.toolFields.sortURL ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };
    $scope.sortStatus = function() {
      $scope.toolFields.sortName = null;
      // $scope.toolFields.sortStatus = null;
      var sorted = [];
      $scope.toolFields.sortStatus = !$scope.toolFields.sortStatus;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.toolFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.toolFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
    };

    $scope.updateEdit = function(item) {
       
      if( item === null || item === undefined ) {
        // $scope.toolFields.inputId = $scope.list.length;
        $scope.toolFields.inputName = '';
        $scope.toolFields.inputDate = '';
        $scope.toolFields.inputDescription = '';
        $scope.toolFields.inputURL = '';
        $scope.toolFields.inputOrder = '';
        $scope.toolFields.inputLive = true;
        $scope.toolFields.inputDeleted = false;
      }
      else {
        $window.console.log('updateEdit()' + JSON.stringify(item));
        $scope.toolFields.inputId = item.slug;
        $scope.toolFields.inputName =        ( item.metafield.name        ? item.metafield.name.value : '');
        $scope.toolFields.inputDate =        ( item.metafield.date        ? item.metafield.date.value : '');
        $scope.toolFields.inputDescription = ( item.metafield.description ? item.metafield.description.value : '');
        $scope.toolFields.inputURL =         ( item.metafield.url         ? item.metafield.url.value : '');
        $scope.toolFields.inputOrder =       ( item.metafield.order     ? item.metafield.order.value : ''); //item.metafield.address.value;
        $scope.toolFields.inputLive =        ( item.metafield.live      ? item.metafield.live.value : true ); //item.metafield.live.value;
        $scope.toolFields.inputDeleted =     ( item.metafield.deleted   ? item.metafield.deleted.value : false ); //item.metafield.live.value;
      }
      $scope.toolFields.loading = false;  
      $rootScope.rootFields.updating = false;  
      $scope.validateFields();   
      $rootScope.updateView();           
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);
        
    };

    $scope.validateFields = function() {
      var valid = true;
      if( $scope.toolFields.inputName === '' ) {
        // TODO update with space removing string check
        // $window.alert('Name cannot be empty');
        valid = false;
      } 
      if( $scope.toolFields.inputURL === '' || $scope.toolFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
        valid = false;
      }

      $scope.toolFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      if( $scope.validateFields() ) {
      if( notify === undefined ) {
        notify = false;
      }
    
        $scope.toolFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.toolFields.inputId ) {
            $scope.toolFields.inputId = $scope.list ? $scope.list.length : 0;
            $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
          // $rootScope.loadPage('tools');
          // $rootScope.loadPage('tools');
        };
        var callback = function() {
          if( $scope.toolFields.fileImage ) {
            $scope.addMedia($scope.toolFields.fileImage, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.toolFields.filePDF ) {
           $scope.addMedia($scope.toolFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }        
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.toolFields.filePDF = file;
        $scope.toolFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.toolFields.fileImage = file;
         $scope.toolFields.inputImageName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.toolFields.filePDF);
      // $window.console.log($scope.toolFields.fileImage);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.toolFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.toolFields.inputFileURL = response.data.media.url;
          
        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects('tools');
        $rootScope.loadPage('tools');        
        
      };
      var data = {
        slug: 'tool-'+$scope.toolFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,         'key': $scope.brochureData.object.name.key,         'type': $scope.brochureData.object.name.type,         'value': $scope.toolFields.inputName },
            { 'title': $scope.brochureData.object.date.title,         'key': $scope.brochureData.object.date.key,         'type': $scope.brochureData.object.date.type,         'value': $scope.toolFields.inputDate },
            { 'title': $scope.brochureData.object.description.title,  'key': $scope.brochureData.object.description.key,  'type': $scope.brochureData.object.description.type,  'value': $scope.toolFields.inputDescription },
            { 'title': $scope.brochureData.object.url.title,          'key': $scope.brochureData.object.url.key,          'type': $scope.brochureData.object.url.type,          'value': $scope.toolFields.inputURL },
            // { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.toolFields.inputFileName },
            // { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.toolFields.inputFileURL },
            // { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': $scope.toolFields.inputImageName },
            // { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': $scope.toolFields.inputImageURL },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.toolFields.inputOrder },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.toolFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.toolFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('tools');
        $rootScope.loadPage('tools');                
      };
      // if( $scope.toolFields.inputURL.toString().toLowerCase().indexOf('http') === -1 ) {
      //   $scope.toolFields.inputURL = 'http://'+$scope.toolFields.inputURL;
      // }

      var data = {
        slug: $scope.toolFields.inputId,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,         'key': $scope.brochureData.object.name.key,         'type': $scope.brochureData.object.name.type,         'value': $scope.toolFields.inputName },
            { 'title': $scope.brochureData.object.date.title,         'key': $scope.brochureData.object.date.key,         'type': $scope.brochureData.object.date.type,         'value': $scope.toolFields.inputDate },
            { 'title': $scope.brochureData.object.description.title,  'key': $scope.brochureData.object.description.key,  'type': $scope.brochureData.object.description.type,  'value': $scope.toolFields.inputDescription },
            { 'title': $scope.brochureData.object.url.title,          'key': $scope.brochureData.object.url.key,          'type': $scope.brochureData.object.url.type,          'value': $scope.toolFields.inputURL },
            // { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.toolFields.inputFileName },
            // { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.toolFields.inputFileURL },
            // { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': $scope.toolFields.inputImageName },
            // { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': $scope.toolFields.inputImageURL },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.toolFields.inputOrder },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.toolFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.toolFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };

    $scope.toggleDelete = function() {
      $scope.toolFields.confirmDelete = !$scope.toolFields.confirmDelete;
      
    };

    $scope.deleteObject = function() {
       $window.console.log('tool.js, deleteObject();');
       $window.console.log($scope.toolFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.toolFields.inputId !== undefined && $scope.toolFields.inputId !== null && $scope.toolFields.inputId !== '' ){
          $scope.toolFields.inputDeleted = true;
          $scope.toolFields.inputLive = false;
          $scope.saveFields(false);
       } 

      
    };
    

    $scope.getObjects = function() {
      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.toolFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.toolFields.disableButtons = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.toolFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));     
          }     
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };

    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        // $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

    

   
    $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});


var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('toolFilter', function() {
  return function(items, brochureData, rootFields) {

    // console.log('toolFilter');
    var debugFilter = false;
    var filtered = [];  
    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;
        // console.log(item);
        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;            
        } 
        else{
          // console.log( item);
          if( debugFilter ) {
           
              console.log( 'item: is missing metafield.deleted, adding now');             
          }
         
          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( rootFields.searchValue !== null ) {  
            
            var text = rootFields.searchValue.toLowerCase();
          console.log('toolFilter: ' + rootFields.searchValue + ' ' + item.metafield.name.value + ' ' + text);
            
            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            else {
              result = false;
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('toolFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});
