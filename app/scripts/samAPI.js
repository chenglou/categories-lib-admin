'use strict';
var $;

var samAPI = { VERSION: 1.0 };

samAPI.testSamURL = 'https://test-services.interact.technology';
samAPI.prodSamURL = 'https://services.interact.technology';

samAPI.CheckSam = function(_string) {
    var result = false;
    if( _string === 'prod' || _string === 'PROD' ) {
        result = samAPI.prodSamURL;
    }
    else if( _string === 'test' || _string === 'TEST' ) {
        result = samAPI.testSamURL;
    }
    else {
        result = false;
    }
    return result;
}; 
samAPI.CheckAuth = function(auth) {
    var result = true;
    if( auth && auth.authtoken ) {
        // TODO:(KYLE) Add more string checks here
        if( auth.authtoken === '' ) {
            result = false;
        }
    }
    return result;
};

samAPI.CheckString = function( _string ) {
    var result = true;
    if( _string.length <= 0 ) {
        result = false;
    }
    return result;
};

samAPI.GetTest = function() {
    // window.alert('samAPI returns YEP');
};

/*  
    sam = 'https://test-services.interact.technology' or https://services.interact.technology;
    auth = response from authtoken callback;
        {
            authtoken: xxxxxxxxxxx,
            error: ??
        }
    data = {
        version: 1,
        type: 'GE' or 'GT',
        key: CompanyID for GT or EventID for GE,
        message: message to send 
    }
*/

samAPI.findUserProfile = function( sam, auth, onDone, onError, onAlways ) {
     // Check incoming data
    var samURL = samAPI.CheckSam(sam);
    // TODO: (KYLE) redo this function + if check
    if( samURL === false ) {
        console.log('Sam url: ' + sam + ' is not valid, exiting findUserProfile();');
        return;
    }
    $.ajax({
    url: samURL + '/rest/user/profile',
    type: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'x-nextinteract-authtoken': auth.authtoken
    },
    })
    .done(function(response, textStatus, jqXHR) {
        window.console.log('samAPI: findUserProfile - ' + JSON.stringify(jqXHR));
        if( onDone ){
            onDone(response);
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        window.console.log('samAPI: findUserProfile - ' + JSON.stringify(jqXHR));
        window.console.log(textStatus);
        window.console.log(errorThrown);
        if( onError ) {
            onError(jqXHR);                
        } 
    })
    .always(function() {
        if( onAlways ) {
            onAlways();
        }
    });
};

samAPI.sendNotification = function(sam, auth, data, onDone, onError, onAlways ) {
    // Check incoming data
    var samURL = samAPI.CheckSam(sam);
    // TODO: (KYLE) redo this function + if check
    if( samURL === false ) {
        console.log('Sam url: ' + sam + ', is not valid, exiting sendNotification();');
        window.alert('Sam url: ' + sam + ', is not valid');
        return;
    }
    
    if( !samAPI.CheckAuth(auth) ){
        console.log('Auth: ' + JSON.stringify(auth) + ', is not valid, exiting sendNotification();');
        window.alert('Auth: ' + JSON.stringify(auth) + ', is not valid');
        return;
    }
    var resultData = true;
    if( data ) {
        if( !data.version || data.version !== 1 ) {
            console.log( 'version not valid');
            resultData = false;
        }
        if( data.type !== undefined) {
            if( data.type !== 'ge' && data.type !== 'gt' ) {
                console.log( 'type: ' + data.type + 'not valid');
                resultData = false;
            }
        }
        if( data.key !== undefined ) {
            if(  !samAPI.CheckString( data.key )  ) {
                console.log( 'key: ' + data.key + 'not valid');
                resultData = false;
            }
        }
       
        if( !data.message || !samAPI.CheckString( data.message )  ) {
            console.log( 'message not valid');
            resultData = false;
        }
    }
    else {
        resultData = false;
    }

    if( resultData ) {
        
    }
    else {
        console.log('Data: ' + JSON.stringify(data) + ', is not valid, exiting sendNotification();');
        window.alert('Data: ' + JSON.stringify(data) + ', is not valid');
        return;
    }
    console.log(data);
    // window.alert('sendNotification(): ' + sam + ' , ' + auth);
    $.ajax({
    url: samURL + '/rest/notification/send',
    type: 'POST',
    data: JSON.stringify(data),
    headers: {
        'Content-Type': 'application/json',
        'x-nextinteract-authtoken': auth.authtoken
    },
    })
    .done(function(response, textStatus, jqXHR) {
         window.console.log(textStatus);
        window.console.log(jqXHR);
        // window.alert('send notification HTTP Request Succeeded: ' + jqXHR.status);
        // window.alert('Notification successfully sent');
        if( onDone ){
            onDone(response);
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        window.console.log(textStatus);        
        window.console.log(errorThrown);
        
        // window.alert('send notification HTTP Request Failed, ' + JSON.stringify(jqXHR));
        if( onError ) {
            onError(jqXHR);                
        } 
    })
    .always(function() {
        if( onAlways ) {
            onAlways();
        }
    });
};

/* 
    Description: 
        Returns Teams that have been defined as "Public" within SAM. 
        NOTE: this currently only applies to MI (not NI yet) and is used for MI Rep Registrations.

    Path:  
        /rest/user/public/teams 

    Transport: 
        HTTPS (prod), HTTP (test)

    HTTP Operation:
        GET
    
    Security: 
        HTTPS(443) transport only - HTTP(80) will be disabled.
    
    Cachable: 
        No 
    
    HTTP Request Headers: 
        x-nextinteract-authtoken: returned from loginUser

    HTTP Request Params/Object: 
        {
           cId:  Company Id (NOTE: the requesting user must be associated with this company via NI or MI),
           regoTeamsOnly: (OPTIONAL) - boolean value (true|false) indicating that Teams should be filtered on whether they relate to a Rego Asset or not
        }
    HTTP Response Params: 
        N/A
    
    HTTP Response Code: 
        200

    HTTP Response Body:
        JSON Reponse:
        [
            {
                "teamId": 22,
                "teamName": "Equity Investments"
            },
            {
                "teamId": 21,
                "teamName": "Property Investments"
            }
        ]
            
*/

samAPI.findPublicTeams = function(sam, auth, data, onDone, onError, onAlways ) {
    // Check incoming data
    var samURL = samAPI.CheckSam(sam);
    // TODO: (KYLE) redo this function + if check
    if( samURL === false ) {
        console.log('Sam url: ' + sam + ', is not valid, exiting findPublicTeams();');
        window.alert('Sam url: ' + sam + ', is not valid');
        return;
    }
    
    if( !samAPI.CheckAuth(auth) ){
        console.log('Auth: ' + JSON.stringify(auth) + ', is not valid, exiting findPublicTeams();');
        window.alert('Auth: ' + JSON.stringify(auth) + ', is not valid');
        return;
    }
    var resultData = true;
    if( data ) {
        if( !data.cId || !samAPI.CheckString( data.cId ) ) {
            console.log( 'cId not valid');
            resultData = false;
        }
        if( data.regoTeamsOnly === undefined ) {
            console.log( 'regoTeamsOnly not valid');
            resultData = false;
        }
    }
    else {
        resultData = false;
    }

    if( resultData ) {
        
    }
    else {
        console.log('Data: ' + JSON.stringify(data) + ', is not valid, exiting findPublicTeams();');
        window.alert('Data: ' + JSON.stringify(data) + ', is not valid');
        return;
    }
    console.log(data);
    // Send AJAX call
    $.ajax({
        url: samURL + '/rest/user/public/teams',
        type: 'GET',
        data: data,
        headers: {
            'x-nextinteract-authtoken': auth.authtoken
        },
    }).done(function(response, textStatus, jqXHR) {
         window.console.log(textStatus);
         window.console.log(jqXHR);
        
        // window.alert('GET Public Teams  HTTP Request Succeeded: ' + jqXHR.status);
        // window.alert(JSON.stringify(data));
        if( onDone ){
            onDone(response);
        }

        // UpdateTeams(data);
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
         window.console.log(textStatus);
         window.console.log(errorThrown);
        // window.alert('GET Public Teams HTTP Request Failed, ' + JSON.stringify(jqXHR));
        if( onError ) {
            onError(jqXHR);                
        } 

        // UpdateTeams([]);
        // window.alert(JSON.stringify(data));
        // window.alert(JSON.stringify(jqXHR));
        // window.alert(JSON.stringify(textStatus));
        // window.alert(JSON.stringify(errorThrown));
    })
    .always(function() {    
        if( onAlways ) {
            onAlways();
        }
    });
};