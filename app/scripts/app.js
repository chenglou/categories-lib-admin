'use strict';
var Cosmic;
var jQuery;
var samAPI;
/**
 * @ngdoc overview
 * @name delegateApp
 * @description
 * # delegateApp
 *
 * Main module of the application.
 */

var delegateApp = angular
  .module('delegateApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngMaterial',
    'ui.bootstrap',
  ])
  .config(function ($routeProvider, $locationProvider) {

  // $mdThemingProvider.theme('default').primaryPalette('green').accentPalette('deep-orange');

    $routeProvider.when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      });
      $routeProvider.when('/category', {
        templateUrl: 'views/category.html',
        controller: 'CategoryCtrl',
        controllerAs: 'category'
      });
      $routeProvider.when('/category/edit:id', {
        templateUrl: 'views/category-edit.html',
        controller: 'CategoryEditCtrl',
        controllerAs: 'category-edit'
      });
      $routeProvider.when('/content', {
        templateUrl: 'views/content.html',
        controller: 'ContentCtrl',
        controllerAs: 'content'
      });
      $routeProvider.when('/content/edit:id', {
        templateUrl: 'views/content-edit.html',
        controller: 'ContentEditCtrl',
        controllerAs: 'content-edit'
      });
      // $routeProvider.when('/itinerary/edit:id', {
      //   templateUrl: 'views/itinerary.html',
      //   controller: 'ItineraryCtrl',
      //   controllerAs: 'itinerary'
      // });
      // $routeProvider.when('/itinerary/search:id', {
      //   templateUrl: 'views/itinerary.html',
      //   controller: 'ItineraryCtrl',
      //   controllerAs: 'itinerary'
      // });
      // $routeProvider.when('/content', {
      //   templateUrl: 'views/itinerary.html',
      //   controller: 'ItineraryCtrl',
      //   controllerAs: 'itinerary'
      // });
      $routeProvider.otherwise({
        redirectTo: '/'
      });
      $locationProvider.hashPrefix('');

  }).run(function ($rootScope, $location, $window, $route, $http, $uibModal) { // , $http  $location ) {

    $rootScope.data = {
      content: [],
      categories: [],
      categoryFilters: [],
    };

    $rootScope.popup = {
      // filters: true,
    };

    $rootScope.location = {
      lastLocation: '',
      newLocation: '',
    };

    $rootScope.fields = {
        containerHeight: 0,
        headerHeight: 0,
        queries: [],
        searchValue: '',
        windowLocation: '',
    };


    // Date picker options
    $rootScope.today = function() {
      $rootScope.dt = new Date();
    };
    $rootScope.today();

  $rootScope.clear = function() {
    $rootScope.dt = null;
  };

  $rootScope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $rootScope.dateOptions = {
    formatYear: 'yy',
    maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  // Disable weekend selection


  $rootScope.toggleMin = function() {
    $rootScope.inlineOptions.minDate = $rootScope.inlineOptions.minDate ? null : new Date();
    $rootScope.dateOptions.minDate = $rootScope.inlineOptions.minDate;
  };

  $rootScope.toggleMin();

  $rootScope.open1 = function() {
    $rootScope.popup1.opened = true;
  };

  $rootScope.open2 = function() {
    $rootScope.popup2.opened = true;
  };

  $rootScope.setDate = function(year, month, day) {
    $rootScope.dt = new Date(year, month, day);
  };

  $rootScope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $rootScope.format = $rootScope.formats[0];
  $rootScope.altInputFormats = ['M!/d!/yyyy'];

  $rootScope.popup1 = {
    opened: false
  };

  $rootScope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $rootScope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $rootScope.events.length; i++) {
        var currentDay = new Date($rootScope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $rootScope.events[i].status;
        }
      }
    }

    return '';
  }


    ////// DATE OPTIONS

      $rootScope.isNavVisible = false;
      $rootScope.isNavActive = function () {
      // angular.forEach(routes, function(route){
      //  $window.console.log(route + ' ' + $location.path());

      if( $location.path() !== '/' ) {
        $rootScope.isNavVisible = true;
        return true;
      }
      else {
        $rootScope.isNavVisible = false;

        return false;
      }
    };

    $rootScope.isSearchVisible = false;
    $rootScope.isSearchActive = function() {
      return $rootScope.isSearchVisible;
    };
    $rootScope.toggleSearch = function() {
      if( $location.path() === '/contacts') {
         $rootScope.isSearchVisible = !$rootScope.isSearchVisible;
        $rootScope.resize();
      }
    };

    $rootScope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl){
        // console.log(event);
        // console.log(newUrl);

        $rootScope.location.newLocation = newUrl;
        $rootScope.location.lastLocation = oldUrl;


        // console.log(oldUrl);
        $rootScope.fields.windowLocation = newUrl;
        // $rootScope.getHeaderHeight();
        // $rootScope.getContainerHeight();
        // itSam.sendPingAnalytic('url-' + newUrl);
        $rootScope.assetData.lastLocation = oldUrl;
        $rootScope.resize();
    });

    $rootScope.$watch('rootFields.searchValue', function( val) {
    // $window.console.log(val);
    // $location.search()
    if( val !== null ) {
      // $rootScope.loadPage('search', val);
    }
    else {
      // $rootScope.closeSearch();
    }
    // else {
    //   $rootScope.loadPage('/all', val);
    // }
    // $rootScope.resize();
  });

  $rootScope.closeSearch = function() {

  };

    // jQuery(window).
    $rootScope.resize = function() {
      // $window.console.log('resize');

      var headerObj = angular.element(document.getElementById('header'))[0];
      var headerMainObj = angular.element(document.getElementById('header-main'))[0];
      var headerNavObj = angular.element(document.getElementById('header-nav'))[0];
      var headerSearchObj = angular.element(document.getElementById('header-search'))[0];
      // var overflowObj = angular.element(document.getElementById('overflow'))[0];

      var windowHeight = $window.innerHeight;
      var headerHeight = 10;

      if( headerMainObj.offsetHeight > 0) {
        headerHeight += headerMainObj.offsetHeight;
      }
      else if( headerNavObj.offsetHeight > 0) {
        headerHeight += headerNavObj.offsetHeight;
      }

      // headerMainObj.offsetHeight;

      // console.log(headerMainObj.offsetHeight);

      if( $rootScope.getQuery('search') ) {
        // console.log('Search ON!');
        // console.log(headerSearchObj.offsetHeight);
        headerHeight += 40;
      }
      else {
        // console.log('Search OFF!');
      }




      $rootScope.rootFields.headerHeight = headerHeight;
      $rootScope.rootFields.overflowHeight = windowHeight - headerHeight - 1;


      // var height;
      // var padTop;
      // var windowHeight = jQuery(window).height();
      // height = windowHeight;
      // $window.console.log($location.path() );
      // if( $location.path() !== '/' ) {
      //   $window.console.log('match');
      //   height -= parseInt(jQuery('.header').css('max-height'));
      //   // height -= parseInt(jQuery('.header').css('margin-bottom'));
      //   height -= parseInt(jQuery('.footer').height());
      //   // height += 20;
      //   // height -= 300;
      //   $window.console.log(height);
      // }
      // else {
      //   height -= parseInt(jQuery('.header').css('min-height'));
      //   // height -= parseInt(jQuery('.header').css('margin-bottom'));
      //   height -= parseInt(jQuery('.footer').height());
      //   height -= 1;
      // }
      // if( $rootScope.checkLocation('search') ) {
      //   // $window.console.log('search visible');
      //   height -= parseInt(jQuery('.search').css('max-height'));
      //   padTop = parseInt(jQuery('.search').css('max-height'));
      // }
      // else {
      //   padTop = 0;
      //   // $window.console.log('search hidden');

      //   // height -= parseInt(jQuery('.search').css('min-heightx'));
      // }
      // console.log(height);
      // // height -= 20;
      // jQuery('.overflow').height(height);

      // console.log(jQuery('.overflow').height());
      // jQuery('.overflow').css('margin-top' , padTop );

      // if( jQuery(window).width() > 1024 ) {
      //   var searchRight = jQuery(window).width() - 1024;
      //   jQuery('.search-button').css('right', searchRight * 0.5);
      //   var backLeft = jQuery(window).width() - 1024;
      //   jQuery('.back-button').css('left', backLeft * 0.5);
      // }
      // else {
      //   jQuery('.search-button').css('right', 10);
      //   jQuery('.back-button').css('left', 10);
      // }
    };

    $rootScope.getContainerHeight = function() {
      var height = $window.innerHeight;

      console.log(height);
      height -= $rootScope.fields.headerHeight;
      $rootScope.fields.containerHeight = height;
      // $rootScope.fields.headerHeight;
      // height
    };

    angular.element($window).bind('resize', function(){
      $rootScope.resize();
      // $rootScope.getHeaderHeight();
      // $rootScope.getContainerHeight();
    });

    $rootScope.rootFields = {
      loading: true,
      updating: true,
      searchValue: null,

      // heights
      overflowHeight: 0,
    };



    $rootScope.navClass = function (page) {
      var currentRoute = $location.path().substring(1) || 'home';
      return page === currentRoute ? 'active' : '';
    };
    $rootScope.loadPage = function(url) {
        var final = '';

        if( url === 'refresh' ) {
              $rootScope.rootFields.updating = true;
          //  final = $rootScope.location.newLocation;
          if( $rootScope.assetData.structure ){
            $rootScope.cosmic.getObjects($rootScope.assetData.structure.objects[0].type_slug);
            $rootScope.cosmic.getObjects($rootScope.assetData.structure.objects[1].type_slug);
          }
          $route.reload();
        }
        else if( url === 'reload' ) {
          $route.reload();
        }
        else if( url === 'home' ) {
          final += '#/';
        }
        else if( url === 'filters' ) {
          $rootScope.popup.filters = !$rootScope.popup.filters;
        }
        else if( url === 'edit' ) {
          var editIndex = $location.path().indexOf('/edit');
          if( editIndex !== -1) {
            final += '#' + $location.path().slice(0,editIndex);
          }
          else {
            final += '#' + $location.path();
          }
          final += '/edit:';
        }
        else if( url === 'back' ) {


          // console.log('lastLocation: ' + $rootScope.assetData.lastLocation);
          if( $rootScope.assetData.lastLocation === null || $rootScope.assetData.lastLocation === undefined || $rootScope.assetData.lastLocation === '' ) {
            final += '#/';
          }
          else if( $location.path().indexOf('edit') !== -1 ) {
            // console.log($location.path());
            var editIndex = $location.path().indexOf('/edit');
            if( editIndex !== -1) {
              final += '#' + $location.path().slice(0,editIndex);
            }
            else {
              final += $rootScope.assetData.lastLocation;
            }
          }
          else {
            final += '#/';
          }
        }
        else {
          final += url;
        }

        return final;
      };
      $rootScope.removeQuery = function(query) {
        // console.log('LoadQuery(): ' + query);
        var final = '';
        if( $rootScope.fields.windowLocation !== '' && $rootScope.fields.windowLocation !== null && $rootScope.fields.windowLocation !== undefined)  {
          final += $rootScope.fields.windowLocation;
          // var queryIndex = final.indexOf('?'+query); // only look for current query
          var queryIndex = final.indexOf('?'); // look for all queries
          if( queryIndex !== -1 ) {
            final = final.slice(0,queryIndex);
          }
          // final += $rootScope.getQuery(query) ? '' : '?'+query;
          // console.log(final);
        }
        else {
          final = '';
        }
        return final;
      };
      $rootScope.addQuery = function( query ) {
        // console.log('LoadQuery(): ' + query);
        var final = '';
        if( $rootScope.fields.windowLocation !== '' && $rootScope.fields.windowLocation !== null && $rootScope.fields.windowLocation !== undefined)  {
          final += $rootScope.fields.windowLocation;
          // var queryIndex = final.indexOf('?'+query); // only look for current query
          var queryIndex = final.indexOf('?'); // look for all queries
          if( queryIndex !== -1 ) {
            final = final.slice(0,queryIndex);
          }
          final += '?'+query;
          // console.log(final);
        }
        else {
          final = '';
        }
        return final;

// {{fields.windowLocation + (getQuery('filters') ? '' : '?filters' ) }}
      };

      $rootScope.loadQuery = function( query ) {
        // console.log('LoadQuery(): ' + query);
        var final = '';
        if( $rootScope.fields.windowLocation !== '' && $rootScope.fields.windowLocation !== null && $rootScope.fields.windowLocation !== undefined)  {
          final += $rootScope.fields.windowLocation;
          // var queryIndex = final.indexOf('?'+query); // only look for current query
          var queryIndex = final.indexOf('?'); // look for all queries
          if( queryIndex !== -1 ) {
            final = final.slice(0,queryIndex);
          }
          final += $rootScope.getQuery(query) ? '' : '?'+query;
          console.log(final);
        }
        else {
          final = '';
        }
        return final;

// {{fields.windowLocation + (getQuery('filters') ? '' : '?filters' ) }}
      };
      $rootScope.getPath = function() {
        return $location.path();
      };
      $rootScope.getQuery = function(id) {
        var result = false;
        if( $rootScope.fields.windowLocation ) {
          // console.log($rootScope.fields.windowLocation);
          var queryIndex = $rootScope.fields.windowLocation.indexOf('?'+id);
          if( queryIndex !== -1 ) {
            result = true;
          }
        }
        return result;
      };

    // $rootScope.loadPage = function(page,id) {
    //   $window.console.log('loadPage("' + page +'");');
    //   // $window.console.log('');
    //   $window.console.log('------------------');
    //   var newLocation = '';

    //   if( page === 'refresh' ) {
    //     page = $location.path();
    //     console.log(page);
    //     // $location.url(page);
    //     $rootScope.lastList = null;
    //     $rootScope.rootFields.updating = true;
    //     $route.reload();

    //   }
    //   else {
    //     if(page === 'back' ) {
    //        $rootScope.lastList = null;
    //       $window.console.log('case: back');
    //       var backSearchIndex = $location.path().indexOf('/search');
    //       var backEditIndex = $location.path().indexOf('/edit');
    //       $rootScope.rootFields.searchValue = null;
    //       if( backSearchIndex !== -1 ) {
    //         newLocation = $location.path().slice(0,backSearchIndex);
    //         // var sliced = $location.path().slice(0,backSearchIndex);
    //         // $location.url(sliced);
    //       }
    //       else if( backEditIndex !== -1) {
    //         newLocation = $location.path().slice(0,backEditIndex);
    //       }
    //       else {
    //         // $location.url('/'+page);
    //         // $rootScope.isSearchVisible = false;
    //       }
    //     }
    //     else if(page.indexOf('search') !== -1 ) {
    //       $window.console.log('case: indexof search');
    //       var searchIndex = $location.path().indexOf('/search');
    //       var editIndex = $location.path().indexOf('/edit');

    //       if( page.indexOf('/') !== 0) {
    //           page = '/'+page;
    //       }
    //       if( id !== undefined) {
    //         if( searchIndex !== -1 ) {
    //           $window.console.log('searchIndex ' + page);
    //           newLocation = $location.path().slice(0,searchIndex);
    //           newLocation += '/search:';
    //           $window.console.log('searchValue:' + id);
    //           newLocation += id;
    //         }
    //         else {
    //           $window.console.log('no index ' + page);
    //           newLocation = $location.path() + page;
    //           newLocation += ':'+id;
    //         }
    //       }
    //       if( id === undefined ) {
    //         if( searchIndex !== -1 ) {
    //           $window.console.log('searchIndex ' + page);
    //         newLocation = $location.path().slice(0,searchIndex);
    //         }
    //         else if( editIndex !== -1 ) {
    //           $window.console.log('editindex ' + page);

    //         newLocation = $location.path().slice(0,editIndex);
    //         newLocation += '/search:';
    //         $window.console.log('searchValue:' + id);
    //         // newLocation += id;
    //         }
    //         else {
    //           $window.console.log('no index ' + page);

    //           newLocation = $location.path() + page;
    //         }
    //       }
    //       else {
    //         $window.console.log('searchIndex ' + page);
    //       }

    //     }
    //     else if( page.indexOf('edit') !== -1 ) {
    //       $window.console.log('case: indexof edit');

    //       var editSearchIndex = $location.path().indexOf('/search');
    //       var editEditIndex = $location.path().indexOf('/edit');
    //       if( page.indexOf('/') !== 0) {
    //           page = '/'+page;
    //       }

    //       if( editSearchIndex !== -1 ) {
    //           // $window.console.log($location.path().slice(0,editSearchIndex) + page);
    //           newLocation = $location.path().slice(0,editSearchIndex) + page;
    //       }
    //       else if( editEditIndex !== -1 ) {
    //           newLocation = $location.path().slice(0,editEditIndex) + '/edit:';
    //       }
    //       else {
    //           // $window.console.log( $location.path() + page );
    //           newLocation = $location.path() + page;
    //       }

    //         // else if( strIndex !== -1 ) {
    //         //   // $window.console.log(strIndex);
    //         //   var sliced = $location.path().slice(0,strIndex);
    //         //   $window.console.log(sliced);
    //         //   $location.url(sliced);
    //         // }
    //         // else {
    //         //   $window.console.log( $location.path()+page);
    //         //   $location.url( $location.path()+page);
    //         //   $rootScope.isSearchVisible = false;
    //         // }
    //     }
    //     else {
    //       $window.console.log('case: else: ' +  $location.path() + page );
    //       newLocation += page;
    //     }

    //     $window.console.log('new window location: ' + newLocation );
    //     $location.url(newLocation);
    //   }

    //   // $window.console.log(callback);

    //   // $rootScope.resize();
    //    $window.console.log('------------------');
    //    $window.console.log('');
    // };

  $rootScope.closeSearch = function() {
    // var final = '';
    // var searchIndex = $location.path().indexOf('/search');
    //   if( searchIndex !== -1 ) {
    //     final = $location.path().slice(0,searchIndex);
    //     $location.url(final);
    //   }
  };
    $rootScope.getLocation = function() {
      // $window.console.log($location.path());
      return $location.path();
    };
    $rootScope.checkLocation = function(keyword) {
      var result = false;
      // var value = keyword.toString().toLowerCase();
      // console.log($location.path() );
      var strIndex = $location.path().indexOf(keyword);
      if( strIndex !== -1 ) {
        result = true;
        // $window.console.log(strIndex);
        // var sliced = $location.path().slice(0,strIndex);
        // $window.console.log(sliced);
        // $location.url(sliced);
      }
      return result;
    };
    $rootScope.safeApply = function (fn) {
      var phase = this.$root.$$phase;
      if (phase === '$apply' || phase === '$digest') {
        if (fn && (typeof (fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
    $rootScope.updateView = function () {
      $rootScope.safeApply(function () {
        // $rootScope.resize();
      });
    };
    $rootScope.toBoolean = function(value) {
      if (value && value.length !== 0) {
        var v = ("" + value).toLowerCase();
        value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
      } else {
        value = false;
      }
      return value;
    };



    // SAM API

    $rootScope.assetData = {
      lastLocation: '',
    };

    $rootScope.interact = {
      config: {
        auth: null,
        user: null
      }
    };
    window.getAuthToken = function(auth) {
      if( auth ) {
         console.log('authtoken: ' + auth.authtoken );
        $rootScope.interact.config.auth = auth;
        $rootScope.interact.findUserProfile();
      }

      // $rootScope.cosmic.getObjects();
    };
    $rootScope.interact.sendNotification = function(data, onDone, onError, onAlways ) {
      if( $rootScope.interact.auth ) {
        jQuery.ajax({
        url: 'https://services.interact.technology/rest/notification/send',
        type: 'POST',
        data: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            'x-nextinteract-authtoken': $rootScope.interact.auth.authtoken
        },
        })
        .done(function(response, textStatus, jqXHR) {
            console.log('send notification HTTP Request Succeeded: ' + jqXHR.status);
            if( onDone ){
                onDone(response);
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log('send notification HTTP Request Failed, ' + JSON.stringify(jqXHR));
            if( onError ) {
              onError(jqXHR);
            }
        })
        .always(function() {
          if( onAlways) {
            onAlways();
          }
        });
      }
};

    $rootScope.loadData = function() {
      $http.get('./resources/data.json').then(function(response){
        // $window.console.log(JSON.stringify(response.data));
        $rootScope.assetData.structure = response.data;
        $rootScope.rootFields.loading = false;

        $rootScope.cosmic.config = response.data.cosmic.config;

        $rootScope.cosmic.getObjects($rootScope.assetData.structure.objects[0].type_slug);
        $rootScope.cosmic.getObjects($rootScope.assetData.structure.objects[1].type_slug);
      });
      $rootScope.loadPage('back');
    };
    $rootScope.interact.findUserProfile = function() {
      var onDone = function (response) {
        console.log(response);
        $rootScope.interact.config.user = response;
        // $rootScope.cosmic.addAnalyticObject('Opened admin asset');
      };
      var onError = function () {
        // debug no internet connection here
      };
      if($rootScope.interact.config.auth) {
        samAPI.findUserProfile( 'prod', $rootScope.interact.config.auth, onDone, onError );
      }
    };
    ////////////

    // COSMIC JS
    $rootScope.cosmic = {
      config: {
        bucket: {
          // slug: "it-actellion",
          // read_key:"BMlyNWzfZhBgEjgbBE6w6R2PrwIOKaH9eIkk7TMXyyg2ZYGW6w",
          // write_key:"ywIY3GWuhF98621PJZezg9F4OpNTelo7y9AEXG39A0Y5Zhq8Yn"
        }
      },
      data: null,
    };
    $rootScope.cosmic.addAnalyticObject = function(message){
      var user = '';
      if($rootScope.interact.config.auth ) {
        user = 'id-'+$rootScope.interact.config.user.uId;
      }
      else {
        user = 'Desktop-';
      }

      var object = {
        write_key: $rootScope.cosmic.config.bucket.write_key,
        type_slug: 'apac-analytic-objects',
        title: user,
        content: '',
        metafields: [
          { 'key':'date', 'type':'text', 'value': Date.now() },
          { 'key':'event', 'type':'text', 'value': message }
        ]
      };
      Cosmic.addObject($rootScope.cosmic.config, object, function(error, response){
        if( error !== false) {
          console.log(error);
        }
        console.log(response);
      });
    };
    $rootScope.cosmic.addMedia = function(file, callback) {
      var formData = new FormData();

      formData.append('media', file);
      formData.append('write_key', $rootScope.cosmic.config.bucket.write_key);

      $http.post('https://api.cosmicjs.com/v1/'+$rootScope.cosmic.config.bucket.slug + '/media/',formData, {
        withCredentials: false,
        headers: {'Content-Type': undefined },
        transformRequest: angular.identity
      })
      .then(function(response){
        if( response )  {
          $window.console.log( '$rootScope.cosmic.addMedia():' + response );
          if( callback) {
            callback(response);
          }
        }
      });
    };

    $rootScope.cosmic.addObject = function(data, callback, notify) {
      console.log('$rootScope.cosmic.addObject();');
      var object = {
        write_key: $rootScope.cosmic.config.bucket.write_key,
        type_slug: data.type_slug,
        title: data.slug,
        content: '',
        metafields: data.metafields,
      };
      console.log(object);
      Cosmic.addObject($rootScope.cosmic.config, object, function(error, response){
        if( error !== false) {
          console.log(error);
        }
        console.log(response);
        if( callback) {
          callback();
          if( notify ) {
            $rootScope.interact.sendNotification({
              version: 2,
              type: 'gt',
              key: [7147],
              message: 'New roster updated',
            });
            // $window.alert('Push notification not currently implemented');
          }
        }
        if( $rootScope.interact.config.user ) {
          $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "addedObject-" + data.slug);
        }
        // $rootScope.cosmic.addAnalyticObject('Added new object: ' + data.type_slug + ' type: ' + data.slug);
      });
    };
    $rootScope.cosmic.editObject = function(data, callback, notify){
      console.log('$rootScope.cosmic.editObject();');
        var object = {
        slug: data.slug,
        write_key: $rootScope.cosmic.config.bucket.write_key,
        metafields: data.metafields,
      };
      Cosmic.editObject($rootScope.cosmic.config, object, function(error, response){
        if( error !== false) {
          console.log(error);
        }
        console.log(response);
        if( callback ) {
          callback();
          if( notify ) {
            $rootScope.interact.sendNotification({
              version: 2,
              type: 'gt',
              key: [7147],
              message: 'Rosters Updated',
            });
            $window.alert('Push notification not currently implemented');
          }
        }
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "editObject-" + data.slug);
        // $rootScope.cosmic.addAnalyticObject('Edit object: ' + data.slug);
        }
      });
    };
    $rootScope.cosmic.updateObject = function(data){
      var object = {
        slug: data.slug,
        write_key: $rootScope.cosmic.config.bucket.write_key,
        metafields: data.metafields,
      };
      Cosmic.editObject($rootScope.cosmic.config, object, function(error, response){
        if( error !== false) {
          console.log(error);
        }
        console.log(response);
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "updateObject-" + data.slug);
        // $rootScope.cosmic.addAnalyticObject('Updated object: ' + data.slug);
        }
    });
    };

    $rootScope.cosmic.deleteObject = function(data, callback){
      var object = {
         write_key: $rootScope.cosmic.config.bucket.write_key,
         slug: data.slug,
      };
      Cosmic.deleteObject($rootScope.cosmic.config, object, function(error, response){
        if( error !== false) {
          console.log(error);
        }
        console.log(response);
        if( callback ) {
          callback();
        }
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "deletedObject-" + data.slug);
        // $rootScope.cosmic.addAnalyticObject('deleted object' + data.slug);
        }
    });
    };
    $rootScope.cosmic.getObjects = function(type, callback){
      // console.log('$rootScope.cosmic.getObjects');
      if( type === 'all' || type === '' ) {
        Cosmic.getObjects($rootScope.cosmic.config, function( error, response ){
          if( error !== false) {
            console.log(error);
          }
          // console.log(response);
          if( callback ) {
            callback(response);
          }
       });
      }
      else {
        var params = {
          type_slug: type,
          limit: 10000,
          skipe: 0
        };
        // if(type === 'rosters') {
        //   params.type_slug = 'roster-objects';
        // }
        // else if( type === 'contacts' ) {
        //   params.type_slug = 'contact-objects';
        // }
        Cosmic.getObjectType($rootScope.cosmic.config, params, function(error, response) {
          $rootScope.lastList = response.objects.all;
          if( error !== false ) {
            console.log(error);
          }
          if( response ) {
            var newObjs = [];
            if( response.objects.all) {
              for( var i = 0; i < response.objects.all.length; i++ ) {
                var item = response.objects.all[i];
                if( item.metafield.deleted && $rootScope.toBoolean(item.metafield.deleted.value) === false  ) {
                  // if( item.metafield.live && $rootScope.toBoolean(item.metafield.live.value) === true  ) {
                    newObjs.push(item);
                  // }
                }
              }
              if( type === $rootScope.assetData.structure.objects[0].type_slug ) {
                $rootScope.data.categories = newObjs;
              }
              else if( type === $rootScope.assetData.structure.objects[1].type_slug ) {
                $rootScope.data.content = newObjs;
              }
            }
          }
          if( callback ) {
            callback(response);
          }
        });
      }
    };
    // Modals
    $rootScope.showFilters = function() {
        $window.console.log("showFilters();");
        var filterModal = $uibModal.open({
            animation: true,
            ariaLabelledBy: "modal-title",
            ariaDescribedBy: "modal-body",
            templateUrl: "views/modals/filters.html",
            controller: "FilterCtrl",
            controllerAs: "$ctrl",
            // size: "sm",
            appendTo: undefined,
            resolve: {
                data: function() {
                  return $rootScope.data;
                }
            }
        });
        filterModal.result.then(function(categoryFilters) {
          console.log(categoryFilters);
          $rootScope.data.categoryFilters = categoryFilters;
          //manually trigger product category filter
        });

    };

    ///////////

  // window.location = 'ni-action:getauthtoken?cb=getAuthToken';
  $rootScope.loadData();
  // $window.getAuthToken();
});

delegateApp.filter('filterMenu', function () {
  return function (items) {
      var filtered = [];
      if( items) {
        for( var i = 0; i < items.length; i++ ) {
          var item = items[i];
          filtered.push(item);
        }
      }
      return filtered;
  };
});
