'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:CategoryEditCtrl
 * @description
 * # CategoryEditCtrl
 * Controller of the delegateApp
 */
var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('CategoryEditCtrl', function($scope, $rootScope, $window, $routeParams) {

    $scope.pageClass = 'page-category-edit';

  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

  $scope.list = [];

     $scope.categoryEditFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      disableSave: false,
   
      confirmDelete: false,
      fieldsValid: false,

      inputName: '',
      inputCategory: '',
      inputFileName: '',
      inputFileURL: '',
      inputOtherURL: '',
      inputOrder: '',      
      inputLive: '',
      inputDeleted: '',

      fileObject:''
    };

    $scope.brochureData = $rootScope.assetData.structure.objects[0];

    

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();       
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.categoryEditFields.loading = false; 
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list); 
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();          
      }
      $rootScope.resize();
    };

    
    $scope.previewPDF = function(item) {
      console.log(item);
        // item.
      if( item ) {
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "previewPDF-" + item);
        }
        window.location = item;
      }
       
    };

    

    $scope.updateEdit = function(item) {
      // $scope.categoryEditFields.loading = true;
      //  $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null || item === undefined ) {
        $scope.categoryEditFields.inputName = '';
        $scope.categoryEditFields.inputOrder = '';
        $scope.categoryEditFields.inputImageName = '';
        $scope.categoryEditFields.inputImageURL = '';
        $scope.categoryEditFields.inputLive = true;
        $scope.categoryEditFields.inputDeleted = false;
      }
      else {
        $scope.categoryEditFields.inputId = item.slug;
        $scope.categoryEditFields.inputName =        ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.categoryEditFields.inputOrder =       ( item.metafield.order      ? item.metafield.order.value : '');
        $scope.categoryEditFields.inputImageName =    ( item.metafield.imageName  ? item.metafield.imageName.value : ''); //item.metafield.email.value;
        $scope.categoryEditFields.inputImageURL =     ( item.metafield.imageURL   ? item.metafield.imageURL.value : ''); //item.metafield.photoURL.value;
        $scope.categoryEditFields.inputLive =        ( item.metafield.live      ? item.metafield.live.value : true ); //item.metafield.live.value;
        $scope.categoryEditFields.inputDeleted =     ( item.metafield.deleted   ? item.metafield.deleted.value : false ); //item.metafield.live.value;
      }
      $scope.categoryEditFields.loading = false;     
      $rootScope.rootFields.updating = false;  
      $scope.validateFields();
      $rootScope.updateView();       
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);
        
    };
    $scope.alertInvalidFields = function() {
      // console.log("validateFields");
      // var valid = true;
      if( $scope.categoryEditFields.inputName === '' ) {
        window.alert('Please fill in Name');
      } 
      // else if( $scope.categoryEditFields.inputOrder <= 0 ) {
      //   window.alert('Please fill in Thumbnail');
      // } 
      // if( $scope.categoryEditFields.input === '' ) {
      //   valid = false;
      // } 
      else if( $scope.categoryEditFields.inputImageName === '' ) {
        window.alert('Please fill in Thumbnail');
      }
      // if( $scope.categoryEditFields.inputURL === '' || $scope.categoryEditFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      // $scope.categoryEditFields.fieldsValid = valid;
      // return valid;
    };

    $scope.validateFields = function() {
      console.log("validateFields");
      var valid = true;
      if( $scope.categoryEditFields.inputName === '' ) {
        valid = false;
      } 
      // if( $scope.categoryEditFields.inputOrder <= 0 ) {
      //   valid = false;
      // } 
      // if( $scope.categoryEditFields.input === '' ) {
      //   valid = false;
      // } 
      if( $scope.categoryEditFields.inputImageName === '' ) {
        valid = false;
      }
      // if( $scope.categoryEditFields.inputURL === '' || $scope.categoryEditFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.categoryEditFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      console.log('category-edit.js saveFields');
      $scope.categoryEditFields.disableSave = !$scope.validateFields();
      if( !$scope.categoryEditFields.disableSave ) {
        if( notify === undefined ) {
          notify = false;
        }
        $scope.categoryEditFields.disableButtons = true;
        $scope.categoryEditFields.disableSave = true;
        var callbackAdd = function() {
          if( !$scope.categoryEditFields.inputId ) {
            $scope.categoryEditFields.inputId = $scope.list ? $scope.list.length : 0;
            $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
        };
        var callback = function() {
          if( $scope.categoryEditFields.fileObject ) {
            $scope.addMedia($scope.categoryEditFields.fileObject, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.categoryEditFields.filePDF ) {
          $scope.addMedia($scope.categoryEditFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }        
      }
      else{
        $scope.alertInvalidFields();
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.categoryEditFields.filePDF = file;
        $scope.categoryEditFields.inputImageName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.categoryEditFields.fileObject = file;
         $scope.categoryEditFields.inputImageName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.categoryEditFields.filePDF);
      // $window.console.log($scope.categoryEditFields.fileObject);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.categoryEditFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.categoryEditFields.inputImageURL = response.data.media.url;
          
        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects('itineraries');
        $window.location.href=$rootScope.loadPage('#/category');
        
      };
      var data = {
        slug: 'dmr-category-'+$scope.categoryEditFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.categoryEditFields.inputName },
            { 'title': $scope.brochureData.object.order.title,      'key': $scope.brochureData.object.order.key,     'type': $scope.brochureData.object.order.type,     'value': $scope.categoryEditFields.inputOrder },
            { 'title': $scope.brochureData.object.imageName.title,   'key': $scope.brochureData.object.imageName.key,  'type': $scope.brochureData.object.imageName.type,  'value': $scope.categoryEditFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,    'key': $scope.brochureData.object.imageURL.key,   'type': $scope.brochureData.object.imageURL.type,   'value': $scope.categoryEditFields.inputImageURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.categoryEditFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.categoryEditFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('itineraries');
        $window.location.href=$rootScope.loadPage('#/category');
      };
      var data = {
        slug: $scope.categoryEditFields.inputId,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.categoryEditFields.inputName },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.categoryEditFields.inputOrder },
            { 'title': $scope.brochureData.object.imageName.title,   'key': $scope.brochureData.object.imageName.key,  'type': $scope.brochureData.object.imageName.type,  'value': $scope.categoryEditFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,    'key': $scope.brochureData.object.imageURL.key,   'type': $scope.brochureData.object.imageURL.type,   'value': $scope.categoryEditFields.inputImageURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.categoryEditFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.categoryEditFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };

    $scope.toggleDelete = function() {
      console.log('toggleDelete');
      $scope.categoryEditFields.confirmDelete = !$scope.categoryEditFields.confirmDelete;
    };

    $scope.deleteObject = function() {
       $window.console.log('itinerary.js, deleteObject();');
       $window.console.log($scope.categoryEditFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.categoryEditFields.inputId !== undefined && $scope.categoryEditFields.inputId !== null && $scope.categoryEditFields.inputId !== '' ){
          $scope.categoryEditFields.inputDeleted = true;
          $scope.categoryEditFields.inputLive = false;
          $scope.saveFields(false);
       } 
    };
    

    $scope.getObjects = function() {
      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.categoryEditFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.categoryEditFields.disableButtons = false;
        $scope.categoryEditFields.disableSave = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.categoryEditFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));     
          }     
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        // $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

      $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});

var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('categoryEditFilter', function() {
  return function(items, brochureData, rootFields) {

    // console.log('itineraryFilter');
    var debugFilter = false;
    var filtered = [];  
    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;
        // console.log(item);
        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;            
        } 
        else{
          // console.log( item);
          if( debugFilter ) {
           
              console.log( 'item: is missing metafield.deleted, adding now');             
          }
         
          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( rootFields.searchValue !== null ) {  
            
            var text = rootFields.searchValue.toLowerCase();
          console.log('itineraryFilter: ' + rootFields.searchValue + ' ' + item.metafield.email.value + ' ' + text);
            
            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            if (item.metafield.email.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            else {
              result = false;
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('itineraryFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});
