'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:CategoryCtrl
 * @description
 * # CategoryCtrl
 * Controller of the delegateApp
 */
var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('CategoryCtrl', function($scope, $rootScope, $window, $routeParams, $timeout) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

    $window.console.log($rootScope.location.lastLocation);
    $window.console.log($rootScope.location.newLocation);

    if( $rootScope.location.lastLocation.indexOf('category') !== -1 || $rootScope.location.newLocation.indexOf('?search=') !== -1) {
    }

    if( $rootScope.location.lastLocation.indexOf('?search=') !== -1 || $rootScope.location.newLocation.indexOf('?search=') !== -1) {
      console.log('Remove page-category');
      $scope.pageClass = 'page-hide';
    }
    else {
      console.log('Add page-category');
      $scope.pageClass = 'page-category';
    }

    // $scope.pageClass = 'page-category';
    // $scope.$on('$locationChangeSuccess', function(event, newURL, oldURL){
    //   $window.console.log('Category Route Change');
    //   $window.console.log(newURL);
    //   $window.console.log(oldURL);
    //   if( oldURL.indexOf('?search=') !== -1 || newURL.indexOf('?search=') !== -1 ) {
    //     $window.console.log('REMOVE CLASS');
    //     $scope.pageClass = '';
    //   }
    // });

    // $window.console.log('add pageclass');

    $scope.list = [];

     $scope.categoryFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      sortName: true,
      sortEmail: null,
      sortStatus: null,
      confirmDelete: false,
      fieldsValid: false,

      inputName: '',
      inputEmail: '',
      inputFileName: '',
      inputFileURL: '',
      inputLive: '',
      inputDeleted: '',

      filePDF: '',
      fileImage:''
    };
    // console.log($rootScope.assetData.structure.objects[0]);
    if( $rootScope.assetData && $rootScope.assetData.structure ) {
      $scope.brochureData = $rootScope.assetData.structure.objects[0];
    }

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.categoryFields.loading = false;
        $timeout(function() {
          $rootScope.rootFields.updating = false;
        }, 500);
        // $window.console.log($scope.list);
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();
      }
      // $rootScope.resize();
    };

    $scope.sortName = function() {
      console.log('$scope.sortName');
      if( $rootScope.data.categories ) {
      // $scope.categoryFields.sortName = null;
      $scope.categoryFields.sortEmail = null;
      $scope.categoryFields.sortStatus = null;

      var sorted = [];
      $scope.categoryFields.sortName = !$scope.categoryFields.sortName;
      sorted = $rootScope.data.categories.sort(function(a,b) {
        var objA = a.metafield.name.value.toLowerCase();
        var objB = b.metafield.name.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.categoryFields.sortName ) {
            return -1;
          }
          else {
             return 1;
          }
        }
        else if( objA > objB) {
          if( $scope.categoryFields.sortName ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.sortEmail = function() {
      if( $scope.list ) {
      $scope.categoryFields.sortName = null;
      // $scope.categoryFields.sortEmail = null;
      $scope.categoryFields.sortStatus = null;

      var sorted = [];
      $scope.categoryFields.sortEmail = !$scope.categoryFields.sortEmail;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.email.value.toLowerCase();
        var objB = b.metafield.email.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.categoryFields.sortEmail ) {
            return -1;
          }
          else {
             return 1;
          }

        }
        else if( objA > objB) {
          if( $scope.categoryFields.sortEmail ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.sortStatus = function() {
      if( $rootScope.data.categories.list ) {
      $scope.categoryFields.sortName = null;
      $scope.categoryFields.sortEmail = null;
      // $scope.categoryFields.sortStatus = null;
      var sorted = [];
      $scope.categoryFields.sortStatus = !$scope.categoryFields.sortStatus;
      sorted = $rootScope.data.categories.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.categoryFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }

        }
        else if( objA > objB) {
          if( $scope.categoryFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.previewPDF = function(item) {
      // console.log(item);
        // item.
      if( item ) {
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "previewPDF-" + item);
        }
        window.location = item;
      }

    };



    $scope.updateEdit = function(item) {
      // $scope.categoryFields.loading = true;
      //  $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null || item === undefined ) {

        // { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.categoryFields.inputName },
        //     { 'title': $scope.brochureData.object.email.title,      'key': $scope.brochureData.object.email.key,     'type': $scope.brochureData.object.email.type,     'value': $scope.categoryFields.inputEmail },
        //     { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.categoryFields.inputFileName },
        //     { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.categoryFields.inputFileURL },
        //     { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.categoryFields.inputLive },
        //     { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.categoryFields.inputDeleted },
        // ]
        $scope.categoryFields.inputId = '';
        $scope.categoryFields.inputName = '';
        $scope.categoryFields.inputOrder = '';
        $scope.categoryFields.inputImageName = '';
        $scope.categoryFields.inputImageURL = '';
        $scope.categoryFields.inputLive = true;
        $scope.categoryFields.inputDeleted = false;
      }
      else {
         $scope.categoryFields.inputId = item.slug;
        $scope.categoryFields.inputName =        ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.categoryFields.inputOrder =       ( item.metafield.order      ? item.metafield.order.value : '');
        $scope.categoryFields.inputImageName =   ( item.metafield.imageName  ? item.metafield.imageName.value : ''); //item.metafield.email.value;
        $scope.categoryFields.inputImageURL =    ( item.metafield.imageURL   ? item.metafield.imageURL.value : ''); //item.metafield.photoURL.value;
        $scope.categoryFields.inputLive =        ( item.metafield.live      ? item.metafield.live.value : true ); //item.metafield.live.value;
        $scope.categoryFields.inputDeleted =     ( item.metafield.deleted   ? item.metafield.deleted.value : false ); //item.metafield.live.value;
      }
      $scope.categoryFields.loading = false;
      $timeout(function() {
        $rootScope.rootFields.updating = false;
      }, 500);
      $scope.validateFields();
      $rootScope.updateView();
    };

    $scope.switchSave = function(item) {
      console.log('switchSave' + item.slug);
        $scope.updateEdit(item);
        $scope.saveFields(false);

    };

    $scope.validateFields = function() {
      var valid = true;
      if( $scope.categoryFields.inputName === '' ) {
        valid = false;
      }
      // if( $scope.categoryFields.input === '' ) {
      //   valid = false;
      // }
      if( $scope.categoryFields.inputImageName === '' ) {
        valid = false;
      }

      // if( $scope.categoryFields.inputURL === '' || $scope.categoryFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.categoryFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      $scope.categoryFields.disableButtons = !$scope.validateFields();
      if( !$scope.categoryFields.disableButtons ) {
        if( notify === undefined ) {
          notify = false;
        }
        $scope.categoryFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.categoryFields.inputId ) {
            $scope.categoryFields.inputId = $scope.list ? $scope.list.length : 0;
            $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
        };
        var callback = function() {
          if( $scope.categoryFields.fileImage ) {
            $scope.addMedia($scope.categoryFields.fileImage, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.categoryFields.filePDF ) {
          $scope.addMedia($scope.categoryFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.categoryFields.filePDF = file;
        $scope.categoryFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.categoryFields.fileImage = file;
         $scope.categoryFields.inputImageName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.categoryFields.filePDF);
      // $window.console.log($scope.categoryFields.fileImage);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.categoryFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.categoryFields.inputFileURL = response.data.media.url;

        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects($scope.brochureData.type_slug);
        $window.location.href=$rootScope.loadPage('#/category');
      };
      var data = {
        slug: 'apac-itinerary-'+$scope.categoryFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.categoryFields.inputName },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.categoryFields.inputOrder },
            { 'title': $scope.brochureData.object.imageName.title,   'key': $scope.brochureData.object.imageName.key,  'type': $scope.brochureData.object.imageName.type,  'value': $scope.categoryFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,    'key': $scope.brochureData.object.imageURL.key,   'type': $scope.brochureData.object.imageURL.type,   'value': $scope.categoryFields.inputImageURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.categoryFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.categoryFields.inputDeleted },
        ]
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('itineraries');
        $window.location.href=$rootScope.loadPage('#/category');
      };
      var data = {
        slug: $scope.categoryFields.inputId,
        metafields: [
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.categoryFields.inputName },
            { 'title': $scope.brochureData.object.order.title,       'key': $scope.brochureData.object.order.key,      'type': $scope.brochureData.object.order.type,      'value': $scope.categoryFields.inputOrder },
            { 'title': $scope.brochureData.object.imageName.title,   'key': $scope.brochureData.object.imageName.key,  'type': $scope.brochureData.object.imageName.type,  'value': $scope.categoryFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,    'key': $scope.brochureData.object.imageURL.key,   'type': $scope.brochureData.object.imageURL.type,   'value': $scope.categoryFields.inputImageURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.categoryFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.categoryFields.inputDeleted },
        ]
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };

    $scope.toggleDelete = function() {
      $scope.categoryFields.confirmDelete = !$scope.categoryFields.confirmDelete;
    };

    $scope.deleteObject = function() {
       $window.console.log('itinerary.js, deleteObject();');
       $window.console.log($scope.categoryFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.categoryFields.inputId !== undefined && $scope.categoryFields.inputId !== null && $scope.categoryFields.inputId !== '' ){
          $scope.categoryFields.inputDeleted = true;
          $scope.categoryFields.inputLive = false;
          $scope.saveFields(false);
       }
    };


    $scope.getObjects = function() {
      // console.log('$scope.getObjects();');
      // console.log($scope.brochureData);

      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.categoryFields.loading = false;
        $timeout(function() {
          $rootScope.rootFields.updating = false;
        }, 500);
        $scope.categoryFields.disableButtons = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.categoryFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));
          }
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        // $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

      $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});

var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('categoryFilter', function() {
  return function(items, fields) {
    console.log('CONTENT FILTER');
    console.log(items);
    console.log(fields);
    var filtered = [];
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;

        // if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
        //   result = false;
        // }
        // else{
          if( fields && fields.searchValue ) {
            var text = fields.searchValue.toLowerCase();
            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
              console.log('test');
              result = true;
            }
            else {
              result = false;
            }
          }
        // }
        if( result ) {
          filtered.push(item);
        }
      }
    }
    return filtered;
  };
});
