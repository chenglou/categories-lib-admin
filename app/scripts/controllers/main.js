'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the delegateApp
 */
angular.module('delegateApp')
  .controller('MainCtrl', function ($scope, $location, $rootScope ) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

    $scope.pageClass = 'page-main';

    // $scope.posterList = $rootScope.posters;

    $rootScope.resize();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});
