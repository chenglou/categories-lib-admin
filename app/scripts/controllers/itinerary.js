'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:ItineraryCtrl
 * @description
 * # ItineraryCtrl
 * Controller of the delegateApp
 */
var delegateApp;
var jQuery;

angular.module('delegateApp')
  .controller('ItineraryCtrl', function($scope, $rootScope, $window, $routeParams) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

  $scope.list = [];

     $scope.itineraryFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      sortName: true,
      sortEmail: null,
      sortStatus: null,
      confirmDelete: false,
      fieldsValid: false,

      inputName: '',
      inputEmail: '',
      inputFileName: '',
      inputFileURL: '',
      inputLive: '',
      inputDeleted: '',

      filePDF: '',
      fileImage:''
    };

     $scope.brochureData = {
      type:'itineraries',
      title: "All Staff Itineraries",
      logo: "brochure-roster.png",
      type_slug: "apac-itineraries",
      object: {
            name:       { "title":"Name",       "key":"name",       "type":"text", "value":"" },
            email:      { "title":"Email",      "key":"email",      "type":"text", "value":"" },
            fileName:   { "title":"File Name",  "key":"fileName",   "type":"text", "value":"" },
            fileURL:    { "title":"File URL",   "key":"fileURL",    "type":"text", "value":"" },
            live:       { "title":"Live",       "key":"live",       "type":"text", "value":"" },
            deleted:    { "title":"Deleted",    "key":"deleted",    "type":"text", "value":"" }
        }
    };

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();       
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.itineraryFields.loading = false; 
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list); 
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();          
      }
      $rootScope.resize();
    };

    $scope.sortName = function() {
      if( $scope.list ) {
      // $scope.itineraryFields.sortName = null;
      $scope.itineraryFields.sortEmail = null;
      $scope.itineraryFields.sortStatus = null;

      var sorted = [];
      $scope.itineraryFields.sortName = !$scope.itineraryFields.sortName;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.name.value.toLowerCase();
        var objB = b.metafield.name.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.itineraryFields.sortName ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.itineraryFields.sortName ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.sortEmail = function() {
      if( $scope.list ) {
      $scope.itineraryFields.sortName = null;
      // $scope.itineraryFields.sortEmail = null;
      $scope.itineraryFields.sortStatus = null;

      var sorted = [];
      $scope.itineraryFields.sortEmail = !$scope.itineraryFields.sortEmail;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.email.value.toLowerCase();
        var objB = b.metafield.email.value.toLowerCase();
        if( objA < objB ) {
          if( $scope.itineraryFields.sortEmail ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.itineraryFields.sortEmail ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.sortStatus = function() {
      if( $scope.list ) {
      $scope.itineraryFields.sortName = null;
      $scope.itineraryFields.sortEmail = null;
      // $scope.itineraryFields.sortStatus = null;
      var sorted = [];
      $scope.itineraryFields.sortStatus = !$scope.itineraryFields.sortStatus;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.itineraryFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.itineraryFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };
    $scope.previewPDF = function(item) {
      console.log(item);
        // item.
      if( item ) {
        if( $rootScope.interact.config.user ) {
        $rootScope.cosmic.addAnalyticObject("user-" + $rootScope.interact.config.user.uId + "." + "asset-admin" + "." + "previewPDF-" + item);
        }
        window.location = item;
      }
       
    };

    

    $scope.updateEdit = function(item) {
      // $scope.itineraryFields.loading = true;
      //  $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null || item === undefined ) {
        $scope.itineraryFields.inputId = '';
        $scope.itineraryFields.inputName = '';
        $scope.itineraryFields.inputEmail = '';
        $scope.itineraryFields.inputFileName = '';
        $scope.itineraryFields.inputFileURL = '';
        $scope.itineraryFields.inputLive = true;
        $scope.itineraryFields.inputDeleted = false;
      }
      else {
         $scope.itineraryFields.inputId = item.slug;
        $scope.itineraryFields.inputName =        ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.itineraryFields.inputEmail =       ( item.metafield.email      ? item.metafield.email.value : '');
        $scope.itineraryFields.inputFileName =    ( item.metafield.fileName  ? item.metafield.fileName.value : ''); //item.metafield.email.value;
        $scope.itineraryFields.inputFileURL =     ( item.metafield.fileURL   ? item.metafield.fileURL.value : ''); //item.metafield.photoURL.value;
        $scope.itineraryFields.inputLive =        ( item.metafield.live      ? item.metafield.live.value : true ); //item.metafield.live.value;
        $scope.itineraryFields.inputDeleted =     ( item.metafield.deleted   ? item.metafield.deleted.value : false ); //item.metafield.live.value;
      }
      $scope.itineraryFields.loading = false;     
      $rootScope.rootFields.updating = false;  
      $scope.validateFields();
      $rootScope.updateView();       
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);
        
    };

    $scope.validateFields = function() {
      var valid = true;
      if( $scope.itineraryFields.inputName === '' ) {
        valid = false;
      } 
      if( $scope.itineraryFields.inputEmail === '' ) {
        valid = false;
      } 
      if( $scope.itineraryFields.inputDate === '' ) {
        valid = false;
      }
      // if( $scope.itineraryFields.inputURL === '' || $scope.itineraryFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.itineraryFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      if( $scope.validateFields() ) {
        if( notify === undefined ) {
          notify = false;
        }
        $scope.itineraryFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.itineraryFields.inputId ) {
            $scope.itineraryFields.inputId = $scope.list ? $scope.list.length : 0;
            $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
        };
        var callback = function() {
          if( $scope.itineraryFields.fileImage ) {
            $scope.addMedia($scope.itineraryFields.fileImage, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.itineraryFields.filePDF ) {
          $scope.addMedia($scope.itineraryFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }        
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.itineraryFields.filePDF = file;
        $scope.itineraryFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.itineraryFields.fileImage = file;
         $scope.itineraryFields.inputImageName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.itineraryFields.filePDF);
      // $window.console.log($scope.itineraryFields.fileImage);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.itineraryFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.itineraryFields.inputFileURL = response.data.media.url;
          
        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects('itineraries');
        $rootScope.loadPage('itinerary');
      };
      var data = {
        slug: 'apac-itinerary-'+$scope.itineraryFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.itineraryFields.inputName },
            { 'title': $scope.brochureData.object.email.title,      'key': $scope.brochureData.object.email.key,     'type': $scope.brochureData.object.email.type,     'value': $scope.itineraryFields.inputEmail },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.itineraryFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.itineraryFields.inputFileURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.itineraryFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,    'key': $scope.brochureData.object.deleted.key,   'type': $scope.brochureData.object.deleted.type,   'value': $scope.itineraryFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.addObject(data, callback, notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('itineraries');
        $rootScope.loadPage('itinerary');
      };
      var data = {
        slug: $scope.itineraryFields.inputId,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.itineraryFields.inputName },
            { 'title': $scope.brochureData.object.email.title,       'key': $scope.brochureData.object.email.key,      'type': $scope.brochureData.object.email.type,      'value': $scope.itineraryFields.inputEmail },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.itineraryFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.itineraryFields.inputFileURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.itineraryFields.inputLive },
            { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': $scope.itineraryFields.inputDeleted },
        ] 
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };

    $scope.toggleDelete = function() {
      $scope.itineraryFields.confirmDelete = !$scope.itineraryFields.confirmDelete;
    };

    $scope.deleteObject = function() {
       $window.console.log('itinerary.js, deleteObject();');
       $window.console.log($scope.itineraryFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.itineraryFields.inputId !== undefined && $scope.itineraryFields.inputId !== null && $scope.itineraryFields.inputId !== '' ){
          $scope.itineraryFields.inputDeleted = true;
          $scope.itineraryFields.inputLive = false;
          $scope.saveFields(false);
       } 
    };
    

    $scope.getObjects = function() {
      var callback = function(response) {
        // console.log(response);
        $scope.list = response.objects.all;
        $scope.itineraryFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.itineraryFields.disableButtons = false;

        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.itineraryFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));     
          }     
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      for( var i = 0; i < $scope.list.length; i++ ) {
        // $window.console.log($scope.list[i]);
        if( ':'+$scope.list[i].slug === id ) {
          return $scope.list[i];
        }
      }
    };

      $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});

var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('itineraryFilter', function() {
  return function(items, brochureData, rootFields) {

    // console.log('itineraryFilter');
    var debugFilter = false;
    var filtered = [];  
    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;
        // console.log(item);
        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;            
        } 
        else{
          // console.log( item);
          if( debugFilter ) {
           
              console.log( 'item: is missing metafield.deleted, adding now');             
          }
         
          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( rootFields.searchValue !== null ) {  
            
            var text = rootFields.searchValue.toLowerCase();
          console.log('itineraryFilter: ' + rootFields.searchValue + ' ' + item.metafield.email.value + ' ' + text);
            
            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            if (item.metafield.email.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            else {
              result = false;
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('itineraryFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});
