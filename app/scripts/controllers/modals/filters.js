'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:FilterCtrl
 * @description
 * # FilterCtrl
 * Controller of the posterAppApp
 */
angular.module('delegateApp')
  .controller('FilterCtrl', function ($uibModalInstance, data ) {
  // .controller('ProfileCtrl', function ($scope, $rootScope, $location, $window, $http ) {
  var $ctrl = this;

  $ctrl.list = [];
  $ctrl.data = data;
  $ctrl.filterAll = false;
  
  $ctrl.checkFilters = function() {
    console.log($ctrl.data.categoryFilters);
    for( var c = 0; c < $ctrl.data.categories.length; c++ ) {
      var item = {
        slug: $ctrl.data.categories[c].slug,
        name: $ctrl.data.categories[c].metafield.name.value,
        value: true,
      };
      for( var f = 0; f < $ctrl.data.categoryFilters.length; f++ ) {
        var filter = $ctrl.data.categoryFilters[f];
        if( item.slug === filter ) {
          console.log('match found');
          item.value = false;
        } 
      }
      $ctrl.list.push(item);
    }
  
                   $ctrl.list.sort(function (a, b) {
      if (a.name < b.name) return -1;
      else if (a.name > b.name) return 1;
      return 0;
    });
  };

  $ctrl.closeFilters = function() {
    console.log('closeFilters();');
    // $ctrl.user.live = !$ctrl.user.live;
    $uibModalInstance.close($ctrl.data.categoryFilters);
  };
  // $ctrl.toggleAllFilters = function() {
  //   console.log('allFilters();');
  //   $ctrl.data.categoryFilters = [];
  //   for( var i = 0; i < $ctrl.data.length; i++ ) {
  //     var item  = $ctrl.data[i];
  //     if( $ctrl.filterAll ) {
  //       $ctrl.filterList.push(item.slug);
  //     }
  //   }
  // };
  $ctrl.filterClick = function(item) {
    console.log(item);
    if( item.slug ) {
      var found = false;
      for( var i = 0; i < $ctrl.data.categoryFilters.length; i++ ) {
        var filter = $ctrl.data.categoryFilters[i];

        if( item.slug === filter ) {
          found = true;
        }
      }
      if( found ) {
        if( item.value ) {
          var index = $ctrl.data.categoryFilters.indexOf(item.slug);
          if( index > -1 ) {
            $ctrl.data.categoryFilters = $ctrl.data.categoryFilters.splice(index, 1);
          }
        }
      }
      else {
        if( !item.value ) {
          $ctrl.data.categoryFilters.push(item.slug);
        }
      }
    }
    console.log($ctrl.data.categoryFilters);
  };

  $ctrl.clearFilters = function() {
    console.log('hideProfile();');
    $ctrl.data.categoryFilters = [];
    $uibModalInstance.close($ctrl.data.categoryFilters);
  };

  $ctrl.checkFilters();
});
