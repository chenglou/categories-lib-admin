'use strict';

/**
 * @ngdoc function
 * @name delegateApp.controller:GuidelineCtrl
 * @description
 * # GuidelineCtrl
 * Controller of the delegateApp
 */
var delegateApp;

angular.module('delegateApp')
  .controller('GuidelineCtrl', function($scope, $rootScope, $window, $routeParams) {
  // .controller('MainCtrl', function ($scope, $rootScope, $location, $window, $http ) {

    $scope.list = [];

     $scope.guidelineFields = {
      editVisible: false,
      loading: true,
      disableButtons: false,
      sortName: true,
      sortStatus: null,
      confirmDelete: false,
      fieldsValid: false,

      inputId: '',
      inputName: '',
      inputDate: '',
      inputFileName: '',
      inputFileURL: '',
      inputImageName: '',
      inputImageURL: '',
      inputOrder: '',
      inputLive: '',
      inputDeleted: '',

      filePDF: '',
      fileImage:''
    };

     $scope.brochureData = {
      type:'guidelines',
      title: "All Staff Guidelines",
      logo: "brochure-guidelines.png",
      type_slug: "guideline-objects",
      object: {
            name:       { "title":"Name",       "key":"name",       "type":"text", "value":"" },
            date:       { "title":"Date",       "key":"date",       "type":"text", "value":"" },
            fileName:   { "title":"File Name",  "key":"fileName",   "type":"text", "value":"" },
            fileURL:    { "title":"File URL",   "key":"fileURL",    "type":"text", "value":"" },
            imageName:  { "title":"Image Name", "key":"imageName",  "type":"text", "value":"" },
            imageURL:   { "title":"Image URL",  "key":"imageURL",   "type":"text", "value":"" },
            order:      { "title":"List order", "key":"order",      "type":"text", "value":"" },
            live:       { "title":"Status",     "key":"live",       "type":"text", "value":"" },
            deleted:    { "title":"Deleted",    "key":"deleted",    "type":"text", "value":"" }
        }
    };

    $scope.onLoad = function() {
      if( $rootScope.checkLocation('edit')){
        $scope.getObjects();       
      }
      else if( $rootScope.lastList !== undefined && $rootScope.lastList !== null) {
        $scope.guidelineFields.loading = false; 
        $rootScope.rootFields.updating = false;
        $window.console.log($scope.list); 
        $scope.list = $rootScope.lastList;
      }
      else {
        $scope.getObjects();          
      }
      $rootScope.resize();
    };

  
     $scope.sortName = function() {
      if( $scope.list ) {
        // $scope.guidelineFields.sortName = null;
        $scope.guidelineFields.sortStatus = null;
        var sorted = [];
        $scope.guidelineFields.sortName = !$scope.guidelineFields.sortName;
        sorted = $scope.list.sort(function(a,b) {
          var objA = a.metafield.name.value.toLowerCase();
          var objB = b.metafield.name.value.toLowerCase();
          if( objA < objB ) {
            if( $scope.guidelineFields.sortName ) {
              return -1;
            }
            else {
              return 1;
            }
          
          }
          else if( objA > objB) {
            if( $scope.guidelineFields.sortName ) {
              return 1;
            }
            else {
              return -1;
            }
          }
          else {
            return 0;
          }
        });
      }      
    };
    $scope.sortStatus = function() {
      if( $scope.list ) {
      $scope.guidelineFields.sortName = null;
      // $scope.guidelineFields.sortStatus = null;
      var sorted = [];
      $scope.guidelineFields.sortStatus = !$scope.guidelineFields.sortStatus;
      sorted = $scope.list.sort(function(a,b) {
        var objA = a.metafield.live.value;
        var objB = b.metafield.live.value;
        if( objA < objB ) {
          if( $scope.guidelineFields.sortStatus ) {
            return -1;
          }
          else {
             return 1;
          }
         
        }
        else if( objA > objB) {
          if( $scope.guidelineFields.sortStatus ) {
            return 1;
          }
          else {
             return -1;
          }
        }
        else {
          return 0;
        }
      });
      }
    };

    $scope.updateEdit = function(item) {
      //  $window.console.log('updateEdit()' + JSON.stringify(item));
      if( item === null || item === undefined ) {
        // $scope.guidelineFields.inputId = $scope.list.length;
        $scope.guidelineFields.inputName = '';
        $scope.guidelineFields.inputDate = '';
        $scope.guidelineFields.inputFileName = '';
        $scope.guidelineFields.inputFileURL = '';
        $scope.guidelineFields.inputImageName = '';
        $scope.guidelineFields.inputImageURL = '';
        $scope.guidelineFields.inputOrder = '';
        $scope.guidelineFields.inputLive = true;
        $scope.guidelineFields.inputDeleted = false;

      }
      else {
        $scope.guidelineFields.inputId = item.slug;
        $scope.guidelineFields.inputName =        ( item.metafield.name      ? item.metafield.name.value : '');
        $scope.guidelineFields.inputDate =        ( item.metafield.date      ? item.metafield.date.value : ''); //item.metafield.mobile.value;
        $scope.guidelineFields.inputFileName =    ( item.metafield.fileName  ? item.metafield.fileName.value : ''); //item.metafield.email.value;
        $scope.guidelineFields.inputFileURL =     ( item.metafield.fileURL   ? item.metafield.fileURL.value : ''); //item.metafield.photoURL.value;
        $scope.guidelineFields.inputImageName =   ( item.metafield.imageName ? item.metafield.imageName.value : ''); //item.metafield.photoFileName.value;
        $scope.guidelineFields.inputImageURL =    ( item.metafield.imageURL  ? item.metafield.imageURL.value : ''); //item.metafield.address.value;
        $scope.guidelineFields.inputOrder =       ( item.metafield.order     ? item.metafield.order.value : ''); //item.metafield.live.value;
        $scope.guidelineFields.inputLive =        ( item.metafield.live      ? item.metafield.live.value : true); //item.metafield.live.value;
        $scope.guidelineFields.inputDeleted =     ( item.metafield.deleted   ? item.metafield.deleted.value : false); //item.metafield.live.value;
      }
      $scope.guidelineFields.loading = false;  
      $rootScope.rootFields.updating = false;  
      $scope.validateFields();   
      $rootScope.updateView();         
    };

    $scope.switchSave = function(item) {
        $scope.updateEdit(item);
        $scope.saveFields(false);
        
    };

    $scope.validateFields = function() {
      var valid = true;
      if( $scope.guidelineFields.inputName === '' ) {
        valid = false;
      } 
      // if( $scope.guidelineFields.inputURL === '' || $scope.guidelineFields.inputURL.toString().toLowerCase().indexOf('http') === -1  ) {
      //   valid = false;
      // }

      $scope.guidelineFields.fieldsValid = valid;
      return valid;
    };

    $scope.saveFields = function(notify) {
      if( $scope.validateFields() ) {
        if( notify === undefined ) {
          notify = false;
        }
        $scope.guidelineFields.disableButtons = true;
        var callbackAdd = function() {
          if( !$scope.guidelineFields.inputId ) {
          $scope.guidelineFields.inputId = $scope.list ? $scope.list.length : 0;
          $scope.addObject(notify);
          }
          else {
            $scope.editObject(notify);
          }
          $rootScope.loadPage('guidelines');
        };
        var callback = function() {
          if( $scope.guidelineFields.fileImage ) {
            $scope.addMedia($scope.guidelineFields.fileImage, 'image', callbackAdd);
          }
          else {
            callbackAdd();
          }
        };
        if( $scope.guidelineFields.filePDF ) {
           $scope.addMedia($scope.guidelineFields.filePDF, 'pdf', callback);
        }
        else {
          callback();
        }        
      }
    };

    $scope.fileInputChanged = function(type, element) {
      var file = element.files[0];
      $window.console.log(file);
      if( type === 'pdf' && file.type === 'application/pdf' ) {
        $scope.guidelineFields.filePDF = file;
        $scope.guidelineFields.inputFileName = file.name;
      }
      else if( type === 'image' && file.type === 'image/jpeg' || type === 'image' &&  file.type === 'image/png' ) {
         $scope.guidelineFields.fileImage = file;
         $scope.guidelineFields.inputImageName = file.name;
      }
      else {
        $window.alert('The file you uploaded is not a ' + type);
        element.value = null;
      }
      $rootScope.updateView();
      // $window.console.log($scope.guidelineFields.filePDF);
      // $window.console.log($scope.guidelineFields.fileImage);
      // $window.console.log(element);
    };

    $scope.addMedia = function(file, field, _callback) {
      var callback = function(response) {
        $window.console.log(response.data.media.url);
        if( field === 'image') {
          $scope.guidelineFields.inputImageURL = response.data.media.url;
        }
        else if( field === 'pdf' ) {
          $scope.guidelineFields.inputFileURL = response.data.media.url;
          
        }
        else {
          $window.alert('invalid addMedia() field');
        }
        _callback();
      };
      $rootScope.cosmic.addMedia(file,callback);
    };

    $scope.addObject = function(notify) {
      var callback = function() {
        $scope.getObjects('guidelines');
        $rootScope.loadPage('guidelines');    
      };
      var data = {
        slug: 'guideline-'+$scope.guidelineFields.inputId,
        type_slug: $scope.brochureData.type_slug,
        metafields: [ 
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.guidelineFields.inputName },
            // { 'title': $scope.brochureData.object.meeting.title,    'key': $scope.brochureData.object.meeting.key,   'type': $scope.brochureData.object.meeting.type,   'value': $scope.guidelineFields.inputMeeting },
            // { 'title': $scope.brochureData.object.author.title,     'key': $scope.brochureData.object.author.key,    'type': $scope.brochureData.object.author.type,    'value': $scope.guidelineFields.inputAuthor },
            { 'title': $scope.brochureData.object.date.title,       'key': $scope.brochureData.object.date.key,      'type': $scope.brochureData.object.date.type,      'value': $scope.guidelineFields.inputDate },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.guidelineFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.guidelineFields.inputFileURL },
            { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': $scope.guidelineFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': $scope.guidelineFields.inputImageURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.guidelineFields.inputLive },
        ] 
      };
      $rootScope.cosmic.addObject(data, callback,notify);
    };
    $scope.editObject = function(notify) {
      var callback = function() {
        $scope.getObjects('guidelines');
         $rootScope.loadPage('guidelines');  
      };
      var data = {
        slug: $scope.guidelineFields.inputId,
        metafields: [
            { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': $scope.guidelineFields.inputName },
            // { 'title': $scope.brochureData.object.meeting.title,    'key': $scope.brochureData.object.meeting.key,   'type': $scope.brochureData.object.meeting.type,   'value': $scope.guidelineFields.inputMeeting },
            // { 'title': $scope.brochureData.object.author.title,     'key': $scope.brochureData.object.author.key,    'type': $scope.brochureData.object.author.type,    'value': $scope.guidelineFields.inputAuthor },
            { 'title': $scope.brochureData.object.date.title,       'key': $scope.brochureData.object.date.key,      'type': $scope.brochureData.object.date.type,      'value': $scope.guidelineFields.inputDate },
            { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': $scope.guidelineFields.inputFileName },
            { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': $scope.guidelineFields.inputFileURL },
            { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': $scope.guidelineFields.inputImageName },
            { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': $scope.guidelineFields.inputImageURL },
            { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': $scope.guidelineFields.inputLive },
        ]
      };
      $rootScope.cosmic.editObject(data, callback, notify);
    };





    $scope.toggleDelete = function() {
      $scope.guidelineFields.confirmDelete = !$scope.guidelineFields.confirmDelete;
      
    };

    $scope.deleteObject = function() {
       $window.console.log('tool.js, deleteObject();');
       $window.console.log($scope.guidelineFields.inputId.value );
       $scope.toggleDelete();
       if( $scope.guidelineFields.inputId !== undefined && $scope.guidelineFields.inputId !== null && $scope.guidelineFields.inputId !== '' ){
          $scope.guidelineFields.inputDeleted = true;
          $scope.guidelineFields.inputLive = false;
          $scope.saveFields(false);
       } 
    };

    $scope.upgradeObjects = function(response) {
      var list = response.objects.all;
      if( list ) {
   
      for( var i = 0; i < list.length; i++ ) {
        var item = list[i];
        if( item ) {
          if( !item.metafield.deleted ) {
            $window.console.log('Upgrading Object: ' + list[i].slug);
            var data = {
              slug: item.slug,
              metafields: [
                { 'title': $scope.brochureData.object.name.title,       'key': $scope.brochureData.object.name.key,      'type': $scope.brochureData.object.name.type,      'value': item.metafield.name.value },
                { 'title': $scope.brochureData.object.date.title,       'key': $scope.brochureData.object.date.key,      'type': $scope.brochureData.object.date.type,      'value': item.metafield.date.value },
                { 'title': $scope.brochureData.object.fileName.title,   'key': $scope.brochureData.object.fileName.key,  'type': $scope.brochureData.object.fileName.type,  'value': item.metafield.fileName.value },
                { 'title': $scope.brochureData.object.fileURL.title,    'key': $scope.brochureData.object.fileURL.key,   'type': $scope.brochureData.object.fileURL.type,   'value': item.metafield.fileURL.value },
                { 'title': $scope.brochureData.object.imageName.title,  'key': $scope.brochureData.object.imageName.key, 'type': $scope.brochureData.object.imageName.type, 'value': item.metafield.imageName.value },
                { 'title': $scope.brochureData.object.imageURL.title,   'key': $scope.brochureData.object.imageURL.key,  'type': $scope.brochureData.object.imageURL.type,  'value': item.metafield.imageURL.value },
                { 'title': $scope.brochureData.object.live.title,       'key': $scope.brochureData.object.live.key,      'type': $scope.brochureData.object.live.type,      'value': item.metafield.live.value },
                { 'title': $scope.brochureData.object.deleted.title,       'key': $scope.brochureData.object.deleted.key,      'type': $scope.brochureData.object.deleted.type,      'value': false },
              ]
            };
            $rootScope.cosmic.updateObject(data);
          }
        }
      }    
      }
      return list;
    };

    $scope.getObjects = function() {
      var callback = function(response) {
        console.log(response);
        $scope.list = $scope.upgradeObjects(response);
        // $scope.list = response.objects.all;
        $scope.list = response.objects.all;
        $scope.guidelineFields.loading = false;
        $rootScope.rootFields.updating = false;
        $scope.guidelineFields.disableButtons = false;
        // console.log(response.objects.type[$scope.brochureData.type_slug]);//.objects);//.type);// [$scope.brochureData.type_slug]);
        $rootScope.updateView();
        if( $routeParams.id ) {
          $scope.guidelineFields.loading = true;
          $rootScope.rootFields.updating = true;
          if( $routeParams.id === ':') {
            $scope.updateEdit(null);
          }
          else {
            $window.console.log($routeParams.id);
            $scope.updateEdit($scope.getObjectBySlug($routeParams.id));     
          }     
        }
      };
      $rootScope.cosmic.getObjects($scope.brochureData.type_slug,callback);
    };
    $scope.getObjectBySlug = function(id) {
      if( $scope.list ) {
        for( var i = 0; i < $scope.list.length; i++ ) {
          $window.console.log($scope.list[i]);
          if( ':'+$scope.list[i].slug === id ) {
            return $scope.list[i];
          }
        }   
      }
      return null;
    };

    $scope.onLoad();

    $scope.safeApply = function(fn) {
      var phase = this.$root.$$phase;
      if(phase === '$apply' || phase === '$digest') {
        if(fn && (typeof(fn) === 'function')) {
          fn();
        }
      } else {
        this.$apply(fn);
      }
    };
});
  

var toBoolean = function(value) {
  if (value && value.length !== 0) {
    var v = ("" + value).toLowerCase();
    value = !(v === 'f' || v === '0' || v === 'false' || v === 'no' || v === 'n' || v === '[]');
  } else {
    value = false;
  }
  return value;
};

delegateApp.filter('guidelineFilter', function() {
  return function(items, brochureData, rootFields) {

    // console.log('guidelineFilter');
    var debugFilter = false;
    var filtered = [];  
    if( debugFilter ) {
      console.log(brochureData);      
    }
    if( items && items.length > 0 ) {
      for( var i = 0; i < items.length; i++ ){
        var item = items[i];
        var result = true;
        // console.log(item);
        if( item.metafield.deleted !== undefined  && toBoolean( item.metafield.deleted.value ) ) {
          result = false;            
        } 
        else{
          // console.log( item);
          if( debugFilter ) {
           
              console.log( 'item: is missing metafield.deleted, adding now');             
          }
         
          item.metafield.deleted = {
            key: brochureData.object.deleted.key,
            title: brochureData.object.deleted.title,
            type: brochureData.object.deleted.type,
            value: false
          };
          if( rootFields.searchValue !== null ) {  
            
            var text = rootFields.searchValue.toLowerCase();
          console.log('guidelineFilter: ' + rootFields.searchValue + ' ' + item.metafield.name.value + ' ' + text);
            
            console.log(text);
            if (item.metafield.name.value.toLowerCase().indexOf(text) > -1) {
               console.log('test');     
              result = true;
            }
            else {
              result = false;
            }
          }
        }
        if( result ) {
          filtered.push(item);
        }
      }
    }  
    else {
      if( debugFilter ) {    
        console.log('guidelineFilter: items not valid ' + JSON.stringify(items)); 
      }
    }
    return filtered;
  };
});
